//
//  TipCard.swift
//  FlipCardAnimation
//
//  Created by José Ernesto Stelzer Monar on 08/07/15.
//  Copyright (c) 2015 José Ernesto Stelzer Monar. All rights reserved.
//

import UIKit

protocol TipCardDelegate:class {
    func tipCardWillSlide(tipCard : TipCard)
}

@IBDesignable class TipCard: UIView, UIScrollViewDelegate, MBSliderViewDelegate {
    var tipImage : UIImageView!
    var tipScroll : UIScrollView!
    var tipLabel : UITextView!
    var tipShow : Bool = false
    var tipSlider : MBSliderView!
    var tipShowedBefore : Bool = false
    var usedTipSlider: MBSliderView!
    var backView: UIView!
    var imageName : String = ""
    weak var delegate: TipCardDelegate!

    
    @IBInspectable var previewImage: UIImage? = UIImage(named: "blank") {
        didSet {
            tipImage.image = previewImage
        }
    }
    @IBInspectable var previewTip : String = "There is no tip for this image" {
        didSet {
            previewTip = NSLocalizedString(previewTip, comment:"")
            tipLabel.text = previewTip
            tipLabel.font = UIFont(name: tipLabel.font.fontName, size: 40)
        }
    }
    
    
    func viewForZoomingInScrollView (scrollView : UIScrollView) -> UIView? {
    // Return the view that you want to zoom
        return self.tipImage;
    }
    
    func scrollViewDidZoom (scrollView : UIScrollView) {
    // The scroll view has zoomed, so you need to re-center the contents
        self.centerScrollViewContents()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView(self.superview!.frame)
        
        let twoFingerGesture = UITapGestureRecognizer(target: self, action: "scrollViewTwoFingerTapped")
        twoFingerGesture.numberOfTapsRequired = 1;
        twoFingerGesture.numberOfTouchesRequired = 2;
        self.tipScroll.addGestureRecognizer(twoFingerGesture)
      
    }
    override func layoutSubviews() {
        println("layout")
        self.frame = CGRect(x: 25, y: 25, width: self.superview!.frame.width-50, height: self.superview!.frame.height-100)
        let views = self.subviews as! [UIView]
        for view: UIView in views {
            view.removeFromSuperview()
        }
        
        setupView(CGRect(x: 25, y: 25, width: self.superview!.frame.width-50, height: self.superview!.frame.height-100))
        self.layoutIfNeeded()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
       //setupView(super.view)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView(frame)
    }
    
    func setupView(frame : CGRect) {
        backView = UIView(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
        backView.layer.backgroundColor = UIColor.blackColor().CGColor
        backView.alpha = 0.8
        addSubview(backView)
        
        tipScroll = UIScrollView(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height - 50))
        tipScroll.delegate = self
        addSubview(tipScroll)
        
        tipImage = UIImageView(image: previewImage)
        tipImage.hidden = tipShow
        tipScroll.layer.cornerRadius = 10
        tipScroll.addSubview(tipImage)
        
        tipScroll.contentSize = previewImage!.size
        
        tipLabel = UITextView(frame: CGRect(x: 20, y: 20, width: frame.width - 40, height: frame.height - 40))

        tipLabel.text = previewTip
        //tipLabel.sizeToFit()
        tipLabel.hidden = !tipShow
        tipLabel.editable = false
        tipLabel.textAlignment = NSTextAlignment.Center
        tipLabel.font = UIFont(name: tipLabel.font.fontName, size: 40)
        addSubview(tipLabel)
        
        tipSlider = MBSliderView(frame: CGRect(x: (frame.width-270)/2, y:(frame.height - 50) , width: 270, height: 50))
        if self.tipShowedBefore {
            tipSlider.text = NSLocalizedString("Slide to see the tip", comment:"Mensagem se o usuário já pode ver a dica")
        } else {
            tipSlider.text = NSLocalizedString("Slide to get a tip", comment:"Mensagem se o usuário ainda não gastou para ver a dica")
        }
        tipSlider.labelColor = UIColor.whiteColor()
        tipSlider.delegate = self
        tipSlider.layer.backgroundColor = UIColor.darkGrayColor().CGColor
        tipSlider.layer.cornerRadius = 20
        
        addSubview(tipSlider)
        
        
        
//        let sliderPosition = NSLayoutConstraint(item: tipSlider,     attribute: NSLayoutAttribute.CenterX,   relatedBy: NSLayoutRelation.Equal,  toItem: self,    attribute: nil,   multiplier: 1, constant: 0)
        
        var scrollViewFrame = self.tipScroll.frame
        var scaleWidth = scrollViewFrame.size.width / self.tipScroll.contentSize.width
        var scaleHeight = scrollViewFrame.size.height / self.tipScroll.contentSize.height
        var minScale = min(scaleWidth, scaleHeight)
        
        self.tipScroll.minimumZoomScale = minScale
  
        self.tipScroll.maximumZoomScale = 30.0
        self.tipScroll.zoomScale = minScale;
        
        self.centerScrollViewContents()
    }
    
    
    func tapGestureRecognized() {
        println("Click")
        NSLog("click")
        self.removeFromSuperview()
    }
    
    func centerScrollViewContents() {
        var boundsSize = self.tipScroll.bounds.size
        var contentsFrame = self.tipImage.frame;
        
        if (contentsFrame.size.width < boundsSize.width) {
            contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0
        } else {
            contentsFrame.origin.x = 0.0
        }
        
        if (contentsFrame.size.height < boundsSize.height) {
            contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0
        } else {
            contentsFrame.origin.y = 0.0
        }
        
        self.tipImage.frame = contentsFrame;
        
    }
    func scrollViewDoubleTapped(recognizer : UITapGestureRecognizer) {
        NSLog("Double tap")
        var pointInView = recognizer.locationInView(self.tipImage)
        
        // 2
        var newZoomScale = self.tipScroll.zoomScale * 1.5
        newZoomScale = min(newZoomScale, self.tipScroll.maximumZoomScale);
        
        // 3
        var scrollViewSize = self.tipScroll.bounds.size;
        
        var w = scrollViewSize.width / newZoomScale
        var h = scrollViewSize.height / newZoomScale
        var x = pointInView.x - (w / 2.0)
        var y = pointInView.y - (h / 2.0)
        
        var rectToZoomTo = CGRectMake(x, y, w, h);
        
        // 4
        self.tipScroll.zoomToRect(rectToZoomTo, animated: true)
    
    }
    
    func scrollViewTwoFingerTapped() {
        var newZoomScale = self.tipScroll.zoomScale / 1.5
        newZoomScale = max(newZoomScale, self.tipScroll.minimumZoomScale)
        self.tipScroll.setZoomScale(newZoomScale, animated: true)
    }
    
    
    
   
    
    func sliderDidSlide(slideView : MBSliderView) {
    // Customization example
        if !self.tipShow {
            //TODO get tip content and mark as seen
            self.delegate.tipCardWillSlide(self)
            if !self.tipShowedBefore{
                return
            }
        }
        flipImageTipCard()
    }
    
    func flipImageTipCard(){
        
        UIView.transitionWithView(self.tipScroll, duration: 1, options: UIViewAnimationOptions.TransitionFlipFromLeft, animations: { () -> Void in
            if(!self.tipShow) {
                self.tipScroll.hidden = true
                self.tipLabel.hidden = false
                self.tipShow = true
                self.tipSlider.text = NSLocalizedString("Slide to see the photo", comment:"Mensagem para o usuário voltar a ver a foto")
            } else {
                self.tipImage.hidden = false
                self.tipScroll.hidden = true
                self.tipShow = false
                self.tipSlider.text =  NSLocalizedString("Slide to see the tip", comment:"Mensagem se o usuário já pode ver a dica")
            }
            }, completion: nil)
    }

}
