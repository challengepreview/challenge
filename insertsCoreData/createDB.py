#   Formato do arquivo:
#       1 linha: separador dos campos, caso nao tenha esta linha (neste formato) sera' considerado separador como uma ,
#       2 linha: Item para o menu: descricao<separador>imagem Landscape<separador>imagem Portrait
#       n linha: Item para a fase: imagem master<separador>imagem dependente<separador>dica<separador>descricao para a imagem mestre
#   NAO colocar a extensao nos nomes de arquivo de imagem
#   A entrada eh por linha de comando: python3 createDB.py <path to file1 to insert> ... <path to fileM to insert>
#   A saida sera no terminal mesmo: para salvar no arquivo de texto out.txt coloque ">out.txt" no fim do comando de entrada
#   Foi testado na versao 3.4 do python: python3 normalamente

import sys
import os

createMenu = 'menuNovo =  MenuServices.createMenu("{descricao}", imageMenuL: "{imageL}", imageMenuP: "{imageP}")'
createFase = 'PhaseServices.createPhase("{imageM}", dependent: "{imageD}", tip: "{tip}", masterDescription: "{descricaoM}", phaseMenu: menuNovo)'
separador  = ','
translateTipE = ''
translateTipP = ''

if len(sys.argv) == 1:
    print("Uso eh no formato: python3 createDB.py <path to file1 to insert> ... <path to fileM to insert>")
    exit()

for path in sys.argv[1:]:
    if not os.path.isfile(path):
        print("Arquivo nao encontrado:", path)
        print("Uso eh no formato: python3 createDB.py <path to file1 to insert> ... <path to fileM to insert>")
        exit()

for path in sys.argv[1:]:
    fileInput = open(path)

    n = 1#n conta a linha do arquivo
    linha1 = (fileInput.readline()).strip()

    if len(linha1) > 1:
        linha2 = linha1
    else:
        n += 1
        linha2 = (fileInput.readline()).strip()
        separador = linha1

    menuItens = linha2.split(separador)
    if len(menuItens) != 3:
        print("A linha de menu esta com formato errado:", linha2, "no arquivo", path)
        print("O formato eh: descricao<separador>imagem_Landscape<separador>imagem_Portrait")
        exit()

    print("/* criando novo item no menu */")
    print(createMenu.format(descricao = menuItens[0].replace('"','\\"'), imageL= menuItens[1], imageP = menuItens[2]), "\n")

    for linhaN in fileInput.readlines():
        linhaN = linhaN.strip().split(separador)
        n += 1
        if len(linhaN) != 4:
            print("A linha", n, "esta com formato errado:", linhaN, "no arquivo", path)
            print("O formato eh: imagem_master<separador>imagem_dependente<separador>dica<separador>descricao_para_a_imagem_mestre")
            exit()
        print(createFase.format(imageM = linhaN[0], imageD = linhaN[1], tip = linhaN[2].replace('"','\\"'), descricaoM = linhaN[3].replace('"','\\"')))
        translateTipE += '"' + linhaN[2].replace('"','\\"') + '"' + " = " + '"' + linhaN[2].replace('"','\\"') + '"' + ";\n"
        translateTipP += '"' + linhaN[2].replace('"','\\"') + '"' + " = " +  '"  ";\n'

    print()
    fileInput.close()

print("\n"*5)
print(translateTipE)
print("\n"*5)
print(translateTipP)
