\
TV Series\tv_l\tv_l
chuck_tv\Yvonne\She gets her memories suppressed due to a faulty Intersect upload.\Sarah Lisa Bartowski/Yvonne Strahovski
chuck_tv\chuck\He works at a dead end job at the Burbank Buy More in its Nerd Herd division.\Charles Irving Bartowski/Chuck Bartowski/Zachary Levi
chuck_tv\morgan\He best friend opens an email that causes to download into his brain the full contents of a CIA/NSA supercomputer known as the Intersect, which has served as a database for their combined collected intelligence.\Morgan Guillermo Grimes/Joshua Gomez
chuck_tv\casey\He was born by the name Alexander Coburn, but he had to change to become an agent for the NSA. He was also known as "Angel of Death" 'cause a mission in the communist country Costa Gavras, but nowadays he is"Angel of Live".\John Casey/Adam Baldwin
chuck_tv\ellie\The tv show that her appears has her brother's name.\Ellie Bartowski Woodcomb/Sarah Lancaster
Arrow_tv\felicity\She is especialisty on computers and already date two superheroes.\Felicity Smoak/Emily Bett Rickards
Arrow_tv\oliver\For five years he stayed on a hellish island.\Oliver Queen/Stephen Amell
Arrow_tv\diggle\He is a bodyguard, ex-soldier and a member of a superheroes team.\John Diggle/David Ramsey
Arrow_tv\roy\He is/was a vigilante partner with the codename Arsenal.\Roy Harper/Colton Haynes
Arrow_tv\thea\Her brother stayed on a hellish island for five years.\Thea Queen/Willa Holland
Supernatural_tv\sam\He was possessed by the fallen arcanjo Lucifer.\Sam Winchester/Jared Padalecki
Supernatural_tv\dean\He gets the mark of Cain and became a specie of demon.\Dean Winchester/Jensen Ackles
Supernatural_tv\castiel\He is an angel.\Castiel/Misha Collins
Supernatural_tv\crowley\He is a demon.\Crowley/Mark Sheppard
Supernatural_tv\bobby\He is an old friend of John Winchester, and over time evolved into a father-figure for John's sons.\Bobby Singer/Jim Beaver
penny_tv\ives\She is an expert medium and was confused as a crazy person.\Vanessa Ives/Eva Green
penny_tv\murray\He played a Bram Stoker book character.\Sir Malcolm Murray/Timothy Dalton
penny_tv\victor\He played the protagonist of a Mary Shelley's novel.\Victor Frankenstein/Harry Treadaway
penny_tv\gray\He played the protagonist of a Oscar Wilde's novel.\Dorian Gray/Reeve Carney
penny_tv\ethan\He is an American sharpshooter running away from a checkered past.\Ethan Chandler/Josh Hartnett
tbbt_tv\Leonard\He is an experimental physicist.\Leonard Hofstadter/Johnny Galecki
tbbt_tv\cooper\He is a theoretical physicist.\Sheldon Lee Cooper/Jim Parsons
tbbt_tv\Howard\He is a Jewish aerospace engineer and astronaut.\Howard Wolowitz/Simon Helberg
tbbt_tv\Raj\He is an indian astrophysicist.\Rajesh Koothrappali/Kunal Nayyar
tbbt_tv\Penny\She is a unsuccessful actress that was born on a farm near Omaha, Nebraska.\Penny/Kaley Cuoco-Sweeting
americans_tv\elizabeth\She is a Russian spy infiltrated in USA.\Elizabeth Jennings/Keri Russell
americans_tv\philip\He is a Russian spy infiltrated in USA.\Philip Jennings/Matthew Rhys
got_tv\Daenerys\She is introduced as Daenerys Stormborn of the House Targaryen, the First of Her Name, the Unburnt, Queen of Meereen, Queen of the Andals and the Rhoynar and the First Men, Khaleesi of the Great Grass Sea, Breaker of Chains, and Mother of Dragons.\Daenerys Targaryen/Emilia Clarke
got_tv\Melisandre\She is a priestess of the Lord of Light and a close advisor to Stannis Baratheon in his campaign to take the Iron Throne.\Melisandre/Carice van Houten
got_tv\Eddard\He is the head of House Stark, the Lord of Winterfell, Lord Paramount and Warden of the North, and Hand of the King to Robert I Baratheon.\Eddard Stark/Sean Bean
gossip_tv\Dan\He reveals himself to be an anonymous blogger.\Dan Humphrey/Penn Badgley
gossip_tv\Blair\She said: "There's only one queen b**** in this town and that's me".\Blair Waldorf/Leighton Meester
malcon_tv\Malcolm\He said: "You want to know the best part about childhood? At some point, it ends.".\Malcolm Wilkerson/Frankie Muniz
malcon_tv\Lois\She had five kids and is known by her hot temper and controlling behavior.\Lois Wilkerson/Jane Kaczmarek
bones_tv\temperance\She is a forensic anthropologist and works at the Jeffersonian Institute in Washington, D.C.\Temperance Brennan/Emily Deschanel
bones_tv\booth\He is a Special Agent with the FBI and the current FBI liaison to the Jeffersonian.\Seeley Booth/David Boreanaz
