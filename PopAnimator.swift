//
//  PopAnimator.swift
//  BeginnerCook
//
//  Created by Marin Todorov on 2/18/15.
//  Copyright (c) 2015 Underplot ltd. All rights reserved.
//

import UIKit

class PopAnimator: NSObject, UIViewControllerAnimatedTransitioning {
  
  var reverse: Bool = false
  let duration    = 1.0
  var presenting  = true
  var originFrame = CGRect.zeroRect
  
  func transitionDuration(transitionContext: UIViewControllerContextTransitioning)-> NSTimeInterval {
    return duration
  }
  
  func animateTransition(transitionContext: UIViewControllerContextTransitioning) {

    let containerView = transitionContext.containerView()
    
    let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
    let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
    
    let toView = toViewController.view//transitionContext.viewForKey(UITransitionContextToViewKey)!
    let fromView = fromViewController.view
    
    let direction: CGFloat = reverse ? -1 : 1
    
    let herbView = presenting ? toView : fromView
    
    let initialFrame = presenting ? originFrame : herbView.frame
    let finalFrame = presenting ? herbView.frame : originFrame
    
    let xScaleFactor = presenting ?
      initialFrame.width / finalFrame.width :
      finalFrame.width / initialFrame.width
    
    let yScaleFactor = presenting ?
      initialFrame.height / finalFrame.height :
      finalFrame.height / initialFrame.height
    
    let scaleTransform = CGAffineTransformMakeScale(xScaleFactor, yScaleFactor)
    
    if presenting {
      herbView.transform = scaleTransform
      herbView.center = CGPoint(
        x: CGRectGetMidX(initialFrame),
        y: CGRectGetMidY(initialFrame))
      herbView.clipsToBounds = true
    }
    
    containerView.addSubview(toView)
    containerView.bringSubviewToFront(herbView)
    
    UIView.animateWithDuration(duration, delay:0.0,
      usingSpringWithDamping: 0.4,
      initialSpringVelocity: 0.0,
      options: nil,
      animations: {
        herbView.transform = self.presenting ?
          CGAffineTransformIdentity : scaleTransform
        
        herbView.center = CGPoint(x: CGRectGetMidX(finalFrame),
          y: CGRectGetMidY(finalFrame))
        
      }, completion:{_ in
        transitionContext.completeTransition(true)
    })
    
  }
  
}
