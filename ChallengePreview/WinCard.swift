//
//  WinCard.swift
//  LinkImg
//
//  Created by Erick Ricardo Mattos on 29/07/15.
//  Copyright (c) 2015 Erick Ricardo Mattos. All rights reserved.
//

import UIKit

@objc protocol WinCardDelegate: NSObjectProtocol {
                func willExit()
    optional    func shouldReset()
}

class WinCard: UIView {
    private var delegateExit: WinCardDelegate!
    
    private let message: String = NSLocalizedString("You completed!\nAll the connections were found.", comment: "Mensagem para indicar que o level está completo.")
    
    private let reset: String = NSLocalizedString("Tap here to reset this phase.", comment: "Mensagem para resetar o level.")
    
    private var lblMessage:     UILabel!
    private var lblReset:       UILabel!
    private var viewBlur:       UIVisualEffectView!
    private var viewVibrancy:   UIVisualEffectView!
    
    var blurStyle:                      UIBlurEffectStyle   = UIBlurEffectStyle.Dark
    var messageFont:                    String              = "HelveticaNeue-Bold"
    var messageSize:                    CGFloat             = 30
    var resetFont:                      String              = "HelveticaNeue"
    var resetSize:                      CGFloat             = 25
    var resetBackgroundColor:           UIColor             = UIColor(red: 0.35, green: 0.35, blue: 0.35, alpha: 1)
    var resetTextColor:                 UIColor             = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 1)
    var resetBorderColor:               UIColor             = UIColor(red: 0.7, green: 0.7, blue: 0.7, alpha: 1)
    var resetFocusedBackgroundColor:    UIColor             = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
    var resetFocusedTextColor:          UIColor             = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
    var resetFocusedBorderColor:        UIColor             = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
    
    func setupView(){
        /* configura View */
        backgroundColor = UIColor.clearColor()
        layer.zPosition = 1
        
        /* Blur messageground effect */
        let blur = UIBlurEffect(style: blurStyle)
        viewBlur = UIVisualEffectView(effect: blur)
        addSubview(viewBlur)
        
        viewVibrancy = UIVisualEffectView(effect: UIVibrancyEffect(forBlurEffect: blur))
        viewBlur.contentView.addSubview(viewVibrancy)
        
        /* configura o label de mensagem */
        lblMessage = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 30))
        lblMessage.backgroundColor = UIColor.clearColor()
        lblMessage.font = UIFont(name: messageFont, size: messageSize)
        lblMessage.textColor = UIColor.whiteColor()
        lblMessage.text = message
        lblMessage.numberOfLines = 0 /* infinitas linhas */
        lblMessage.textAlignment = NSTextAlignment.Center
        viewVibrancy.contentView.addSubview(lblMessage)
        
        /* configura o label de reset */
        lblReset = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 30))
        lblReset.layer.backgroundColor = self.resetBackgroundColor.CGColor
        lblReset.textColor = self.resetTextColor
        lblReset.font = UIFont(name: resetFont, size: resetSize)
        lblReset.text = reset
        lblReset.numberOfLines = 0 /* infinitas linhas */
        lblReset.textAlignment = NSTextAlignment.Center
        lblReset.layer.borderColor = self.resetBorderColor.CGColor
        lblReset.layer.borderWidth = 3.0
        lblReset.layer.cornerRadius = 15
        lblReset.layer.zPosition = 3
        addSubview(lblReset)
        
        /* configuracao Constrains */
        viewBlur.setTranslatesAutoresizingMaskIntoConstraints(false)
        
        let blurTop        = NSLayoutConstraint(item: viewBlur, attribute: .Top      , relatedBy: .Equal, toItem: self, attribute: .Top,       multiplier:   1, constant: 0)
        let blurBottom     = NSLayoutConstraint(item: viewBlur, attribute: .Bottom   , relatedBy: .Equal, toItem: self, attribute: .Bottom,    multiplier:   1, constant: 0)
        let blurLeading    = NSLayoutConstraint(item: viewBlur, attribute: .Leading  , relatedBy: .Equal, toItem: self, attribute: .Leading,   multiplier:   1, constant: 0)
        let blurTrailing   = NSLayoutConstraint(item: viewBlur, attribute: .Trailing , relatedBy: .Equal, toItem: self, attribute: .Trailing,  multiplier:   1, constant: 0)
        
        addConstraint(blurTop)
        addConstraint(blurBottom)
        addConstraint(blurLeading)
        addConstraint(blurTrailing)
        
        viewVibrancy.setTranslatesAutoresizingMaskIntoConstraints(false)
        
        let vibrancyTop        = NSLayoutConstraint(item: viewVibrancy, attribute: .Top      , relatedBy: .Equal, toItem: viewBlur, attribute: .Top,       multiplier:   1, constant: 0)
        let vibrancyBottom     = NSLayoutConstraint(item: viewVibrancy, attribute: .Bottom   , relatedBy: .Equal, toItem: viewBlur, attribute: .Bottom,    multiplier:   1, constant: 0)
        let vibrancyLeading    = NSLayoutConstraint(item: viewVibrancy, attribute: .Leading  , relatedBy: .Equal, toItem: viewBlur, attribute: .Leading,   multiplier:   1, constant: 0)
        let vibrancyTrailing   = NSLayoutConstraint(item: viewVibrancy, attribute: .Trailing , relatedBy: .Equal, toItem: viewBlur, attribute: .Trailing,  multiplier:   1, constant: 0)
        
        addConstraint(vibrancyTop)
        addConstraint(vibrancyBottom)
        addConstraint(vibrancyLeading)
        addConstraint(vibrancyTrailing)
        
        lblReset.setTranslatesAutoresizingMaskIntoConstraints(false)
        
        let resetBottom     = NSLayoutConstraint(item: lblReset, attribute: .Bottom   , relatedBy: .Equal, toItem: viewVibrancy, attribute: .Bottom,    multiplier:   1, constant: -20)
        let resetCenter    = NSLayoutConstraint(item: lblReset, attribute: .CenterX  , relatedBy: .Equal, toItem: viewVibrancy, attribute: .CenterX,   multiplier:   1, constant: 0)
        let resetWidth    = NSLayoutConstraint(item: lblReset, attribute: .Width , relatedBy: .Equal, toItem: viewVibrancy, attribute: .Width,  multiplier:   0.7, constant: 0)
        let resetHeight   = NSLayoutConstraint(item: lblReset, attribute: .Height , relatedBy: .Equal, toItem: viewVibrancy, attribute: .Height,  multiplier:   0.2, constant: 0)
        
        addConstraint(resetBottom)
        addConstraint(resetCenter)
        addConstraint(resetWidth)
        addConstraint(resetHeight)
        
        lblMessage.setTranslatesAutoresizingMaskIntoConstraints(false)
        
        lblMessage.setContentHuggingPriority(900, forAxis: .Vertical)
        
        let messageCenter     = NSLayoutConstraint(item: lblMessage, attribute: .CenterY,   relatedBy: .Equal   , toItem: viewVibrancy, attribute: .CenterY,       multiplier:   0.8, constant: -10)
        let messageHeight     = NSLayoutConstraint(item: lblMessage, attribute: .Height   , relatedBy: .Equal, toItem: viewVibrancy, attribute: .Height,    multiplier:   0.5, constant: 0)
        let messageLeading    = NSLayoutConstraint(item: lblMessage, attribute: .Leading  , relatedBy: .Equal, toItem: viewVibrancy, attribute: .Leading,   multiplier:   1, constant: 10)
        let messageTrailing   = NSLayoutConstraint(item: lblMessage, attribute: .Trailing , relatedBy: .Equal, toItem: viewVibrancy, attribute: .Trailing,  multiplier:   1, constant: -10)
        
        addConstraint(messageCenter)
        addConstraint(messageHeight)
        addConstraint(messageLeading)
        addConstraint(messageTrailing)
        
        /* Capturar acao para saida */
        var tapGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("handleTapFromView:"))
        self.addGestureRecognizer(tapGestureRecognizer)
        tapGestureRecognizer.delegate = self
        
        /* Capturar acao para reset */
        self.lblReset.userInteractionEnabled = true
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("handleTapFromLabel:"))
        lblReset.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func setDelegate(delegate: WinCardDelegate){
        self.delegateExit = delegate
    }
    
    private func destaqueResetAction(){
        UIView.animateWithDuration(0.1, animations: { () -> Void in
            self.lblReset.layer.backgroundColor = self.resetFocusedBackgroundColor.CGColor
            self.lblReset.layer.borderColor = self.resetFocusedBorderColor.CGColor
            self.lblReset.textColor = self.resetFocusedTextColor
        })
    }
    
    private func removeDestaqueResetAction(){
        UIView.animateWithDuration(0.2, animations: { () -> Void in
            self.lblReset.layer.backgroundColor = self.resetBackgroundColor.CGColor
            self.lblReset.layer.borderColor = self.resetBorderColor.CGColor
            self.lblReset.textColor = self.resetTextColor
        })
    }
    
    var flagComecouNoReset: Bool = false
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        super.touchesBegan(touches, withEvent: event)
        
        let touch:UITouch = touches.first  as! UITouch
        let positionTouch: CGPoint = touch.locationInView(self)
        
        if self.lblReset.frame.contains(positionTouch){
            self.destaqueResetAction()
            self.flagComecouNoReset = true
            return
        }
        self.flagComecouNoReset = false
    }
    
    override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {
        super.touchesMoved(touches, withEvent: event)
        
        let touch:UITouch = touches.first  as! UITouch
        let positionTouch: CGPoint = touch.locationInView(self)
        
        if self.lblReset.frame.contains(positionTouch) && self.flagComecouNoReset{
            self.destaqueResetAction()
        }else{
            UIView.animateWithDuration(0.3, animations: { () -> Void in
                self.removeDestaqueResetAction()
            })
        }
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        super.touchesEnded(touches, withEvent: event)
        
        if self.flagComecouNoReset{
            
            let touch:UITouch = touches.first  as! UITouch
            let positionTouch: CGPoint = touch.locationInView(self)
            
            if self.lblReset.frame.contains(positionTouch){
                self.handleTapFromLabel(nil)
            }
        }
        
        self.flagComecouNoReset = false
        self.removeDestaqueResetAction()
    }
    
}

extension WinCard: UIGestureRecognizerDelegate{
    func handleTapFromView(recognizer: UITapGestureRecognizer!){
        if delegateExit.respondsToSelector(Selector("willExit")) {
            delegateExit.willExit()
        }
    }
    
    func handleTapFromLabel(recognizer: UITapGestureRecognizer!){
        if delegateExit.respondsToSelector(Selector("shouldReset")) {
            delegateExit.shouldReset!()
        }
    }
}









