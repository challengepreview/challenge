//
//  MenuKindCell.swift
//  ChallengePreview
//
//  Created by Erick Ricardo Mattos on 30/06/15.
//  Copyright (c) 2015 Erick Ricardo Mattos. All rights reserved.
//

import UIKit

protocol CollectionCellDelegate {
    func touchBegan(point: CGPoint, withCell cell: UICollectionViewCell)
    func touchMoved(point: CGPoint, withCell cell: UICollectionViewCell)
    func touchEnded(point: CGPoint, withCell cell: UICollectionViewCell)
}

class MenuKindCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var auxView: UIView!
    
    private var delegateTouch: CollectionCellDelegate!
    var myselfKindItem: MenuKindItem!
    
    /* has to be called AFTER set a image to the imageview */
    /*func setupImageView(){
        imageView.removeConstraints(imageView.constraints())
        
        let originalSize: CGSize = imageView.image!.size
        
        if originalSize.height > originalSize.width {
            let r = originalSize.width / originalSize.height
            imageView.setTranslatesAutoresizingMaskIntoConstraints(false)
            
            let imageCenterY    = NSLayoutConstraint(item: imageView, attribute: .CenterY  , relatedBy: .Equal, toItem: auxView   , attribute: .CenterY  , multiplier: 1, constant: 0)
            let imageLeading    = NSLayoutConstraint(item: imageView, attribute: .Leading  , relatedBy: .Equal, toItem: auxView   , attribute: .Leading  , multiplier: 1, constant: 0)
            let imageTrailing   = NSLayoutConstraint(item: imageView, attribute: .Trailing , relatedBy: .Equal, toItem: auxView   , attribute: .Trailing , multiplier: 1, constant: 0)
            let imageRadio      = NSLayoutConstraint(item: imageView, attribute: .Height   , relatedBy: .Equal, toItem: imageView , attribute: .Width    , multiplier: r, constant: 0)
            
            addConstraint(imageCenterY)
            addConstraint(imageLeading)
            addConstraint(imageTrailing)
            addConstraint(imageRadio)
        }else{
            let r = originalSize.height / originalSize.width
            imageView.setTranslatesAutoresizingMaskIntoConstraints(false)
            
            let imageCenterX    = NSLayoutConstraint(item: imageView, attribute: .CenterX  , relatedBy: .Equal, toItem: auxView   , attribute: .CenterX  , multiplier: 1, constant: 0)
            let imageTop        = NSLayoutConstraint(item: imageView, attribute: .Top      , relatedBy: .Equal, toItem: auxView   , attribute: .Top      , multiplier: 1, constant: 0)
            let imageBottom     = NSLayoutConstraint(item: imageView, attribute: .Bottom   , relatedBy: .Equal, toItem: auxView   , attribute: .Bottom   , multiplier: 1, constant: 0)
            let imageRadio      = NSLayoutConstraint(item: imageView, attribute: .Width    , relatedBy: .Equal, toItem: imageView , attribute: .Height   , multiplier: r, constant: 0)
            
            addConstraint(imageCenterX)
            addConstraint(imageTop)
            addConstraint(imageBottom)
            addConstraint(imageRadio)
        }
    }*/
    
    func setDelegateTouch(#delegateTouch: CollectionCellDelegate ){
        self.delegateTouch = delegateTouch
    }
    
    func touchBegan(point: CGPoint){
        if (self.delegateTouch != nil) {
            self.delegateTouch.touchBegan(point, withCell: self)
        }
    }
    
    func touchMoved(point: CGPoint){
        if (self.delegateTouch != nil) {
            self.delegateTouch.touchMoved(point, withCell: self)
        }
    }
    
    func touchEnded(point:CGPoint){
        if (self.delegateTouch != nil) {
            self.delegateTouch.touchEnded(point, withCell: self)
        }
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        super.touchesBegan(touches, withEvent: event)
        
        if (self.delegateTouch != nil) {
            let touch: UITouch = touches.first as! UITouch
            let newPosition = touch.locationInView(self.superview?.superview)/* viewcontroller? */
            self.delegateTouch.touchBegan(newPosition, withCell: self)
            
        }
    }
    
    override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {
        super.touchesMoved(touches, withEvent: event)
        
        if (self.delegateTouch != nil) {
            let touch: UITouch = touches.first as! UITouch
            let newPosition = touch.locationInView(self.superview?.superview)/* viewcontroller? */
            self.delegateTouch.touchMoved(newPosition, withCell: self)
        }
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        super.touchesEnded(touches, withEvent: event)
        
        if (self.delegateTouch != nil) {
            let touch: UITouch = touches.first as! UITouch
            let newPosition = touch.locationInView(self.superview?.superview)/* viewcontroller? */
            self.delegateTouch.touchEnded(newPosition, withCell: self)
        }
    }
}
