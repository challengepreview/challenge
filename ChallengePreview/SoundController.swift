//
//  SoundController.swift
//  ChallengePreview
//
//  Created by Antonio Carlos Guimarães Junior on 7/16/15.
//  Copyright (c) 2015 Erick Ricardo Mattos. All rights reserved.
//

import UIKit
import AVFoundation


class SoundController: NSObject {
    static let sharedInstance = SoundController()
    var soundEnabled: Bool {
        get{
            return NSUserDefaults.standardUserDefaults().boolForKey("soundEnabled")
        }
        set (valor) {
            let userDefaults:NSUserDefaults = NSUserDefaults.standardUserDefaults();
            userDefaults.setBool(valor, forKey: "soundEnabled")
            userDefaults.synchronize()
        }
    }
    
    override init(){
        super.init()
        NSUserDefaults.standardUserDefaults().registerDefaults([false : "soundEnabled"])
    }
    //private var wrongSoundsLevel = 0;
    private var audioPlayer = AVAudioPlayer();

    /*func playWrongSound(){
        if(wrongSoundsLevel == 4){
            wrongSoundsLevel = random() % 4
        }
        let rndS = random() % wrongAudios[wrongSoundsLevel].count
        var alertSound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource(wrongAudios[wrongSoundsLevel][rndS], ofType: "mp3")!)
        AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, error: nil)
        AVAudioSession.sharedInstance().setActive(true, error: nil)
        wrongSoundsLevel += Int(wrongSoundsLevel < 4)
        var error:NSError?
        audioPlayer = AVAudioPlayer(contentsOfURL: alertSound, error: &error)
        audioPlayer.prepareToPlay()
        audioPlayer.play()
    }*/
    
    func playWrongSound(){
        if(!soundEnabled){
            return
        }
        var alertSound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("fail", ofType: "mp3")!)
        AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, error: nil)
        AVAudioSession.sharedInstance().setActive(true, error: nil)
        var error:NSError?
        audioPlayer = AVAudioPlayer(contentsOfURL: alertSound, error: &error)
        audioPlayer.prepareToPlay()
        audioPlayer.play()
    }
    
    func playRightSound() {
        if(!soundEnabled){
            return
        }
        var alertSound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("right", ofType: "mp3")!)
        AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, error: nil)
        AVAudioSession.sharedInstance().setActive(true, error: nil)
        var error:NSError?
        audioPlayer = AVAudioPlayer(contentsOfURL: alertSound, error: &error)
        audioPlayer.prepareToPlay()
        audioPlayer.play()
    }
    
}
