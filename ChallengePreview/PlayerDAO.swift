//
//  PlayerDAO.swift
//  ChallengePreview
//
//  Created by Erick Ricardo Mattos on 14/07/15.
//  Copyright (c) 2015 Erick Ricardo Mattos. All rights reserved.
//

import Foundation
import CoreData

protocol PlayerProtocolObserver: NSObjectProtocol{
    func setScore(newScore: Int)
    func setCredit(newCredit: Int)
    func isMe(other: PlayerProtocolObserver) -> Bool
}

class PlayerDAO {
    
    static private let player   = PlayerDAO.getPlayer()
    static private var playerObservers: [PlayerProtocolObserver] = [PlayerProtocolObserver]()
    
    static func addObserverPlayer(observerP: PlayerProtocolObserver){
        playerObservers.append(observerP)
        observerP.setScore(getScore())
        observerP.setCredit(getCredit())
    }
    
    static func removeObserverPlayer(observerP: PlayerProtocolObserver){
        playerObservers = playerObservers.filter({!$0.isMe(observerP)})
    }
    
    static func changeScore(s: Int){
        var error:NSErrorPointer = nil
        PlayerDAO.player.setValue(NSNumber(integer: s), forKey: "score")
        
        DatabaseManager.sharedInstance.managedObjectContext?.save(error)
        if (error != nil)
        {
            // log error
            print(error)
        }
        
        for ob in playerObservers.filter({$0.respondsToSelector(Selector("setScore:"))}){
            ob.setScore(s)
        }
    }
    
    static func changeCredit(c: Int){
        var error:NSErrorPointer = nil
        PlayerDAO.player.setValue(NSNumber(integer: c), forKey: "credit")
        
        DatabaseManager.sharedInstance.managedObjectContext?.save(error)
        if (error != nil)
        {
            // log error
            print(error)
        }
        
        for ob in playerObservers.filter({$0.respondsToSelector(Selector("setCredit:"))}){
            ob.setCredit(c)
        }
    }
    
    static func changeValues(s: Int, c: Int){
        var error:NSErrorPointer = nil
        
        PlayerDAO.player.setValue( NSNumber(integer: s), forKey: "score")
        PlayerDAO.player.setValue(NSNumber(integer: c), forKey: "credit")
        
        DatabaseManager.sharedInstance.managedObjectContext?.save(error)
        if (error != nil)
        {
            // log error
            print(error)
        }
        
        for ob in playerObservers.filter({$0.respondsToSelector(Selector("setCredit:"))}){
            ob.setCredit(c)
        }
        for ob in playerObservers.filter({$0.respondsToSelector(Selector("setScore:"))}){
            ob.setScore(s)
        }
    }
    
    static func getCredit() -> Int{
        return player.credit.integerValue
    }
    
    static func getScore() -> Int{
        return player.score.integerValue
    }
    
    static let APhase  = 200
    static let AOthers = 50
    static let custShowCompletedImageListForMaster = PlayerDAO.calculateTakeTip()
    
    static func getPlayer() -> Player
    {
        // creating fetch request
        let request = NSFetchRequest(entityName: "Player")
        
        // perform search
        var error:NSErrorPointer = nil
        let results:[Player] = DatabaseManager.sharedInstance.managedObjectContext?.executeFetchRequest(request, error: error) as! [Player]
        
        return results[0]
    }
    
    static func releasePhasePay(phaseName: String) -> Bool {
        let c_x = PlayerDAO.calculatePayPhase()
        
        if c_x <= PlayerDAO.player.credit.integerValue {
            if MenuDAO.unlockPhase(phaseName){
                var error:NSErrorPointer = nil
                PlayerDAO.player.setValue(NSNumber(integer: PlayerDAO.player.numberPhasePay.integerValue + 1), forKey: "numberPhasePay")
                
                DatabaseManager.sharedInstance.managedObjectContext?.save(error)
                
                if (error != nil)
                {
                    // log error
                    print(error)
                }
                
                takeCX(c_x)
                return true
            }
        }
        return false
    }
    
    static func calculateTakeX(x: Int, A: Int) -> Int{
        return Int(ceil(CGFloat(A)*(1 - exp(CGFloat(-x)/15))))
    }
    
    static func calculateTakeTip() -> Int{
        return 20
    }
    
    static func calculatePayPhase() -> Int {
        return PlayerDAO.calculateTakeX(PlayerDAO.player.numberPhasePay.integerValue + 1, A: PlayerDAO.APhase)
    }
    
    static func takeCX(c_x: Int){
        changeCredit(PlayerDAO.player.credit.integerValue - c_x)
    }
    
    static func updateValues(score: Int, credit: Int){
        changeValues(score, c: credit)
    }
    
    static func updateValues(score: Int){
        changeScore(score)
    }
    
    static func requestMoreCredit(quantityFinal: Int) -> Bool{
        println("passei pela sugestao de compra de mais credito")
        return false
    }
    
    static func insert(objectToBeInserted:Player)
    {
        // insert element into context
        DatabaseManager.sharedInstance.managedObjectContext?.insertObject(objectToBeInserted)
        
        // save context
        var error:NSErrorPointer = nil
        DatabaseManager.sharedInstance.managedObjectContext?.save(error)
        if (error != nil)
        {
            // log error
            print(error)
        }
    }
}