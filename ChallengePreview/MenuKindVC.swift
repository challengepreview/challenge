//
//  MenuKindVC.swift
//  ChallengePreview
//
//  Created by Erick Ricardo Mattos on 01/07/15.
//  Copyright (c) 2015 Erick Ricardo Mattos. All rights reserved.
//

import UIKit

enum Direction: Int{
    case None   = 00
    case Up     = -1
    case Down   = 01
    case Left   = -2
    case Right  = 02
}

class MenuKindVC: UIViewController, UIViewControllerTransitioningDelegate  {
    
    private let velocityAutoScroll: CGFloat = 50
    let transition = PopAnimator()
    var selectedImage: UIImageView?
    var fundoView: UIImageView!
    var scrollView: UIMenuScroll!
    var volumeView: UIView!
    var volumeImageView: UIImageView!
    /* Area de design */
    /* unidades basicas */
    private var vh: CGFloat {
        get{
            return self.view.frame.size.height
        }
    }
    private var vw: CGFloat {
        get{
            return self.view.frame.size.width
        }
    }
    private var vmin: CGFloat {
        get{
            return self.view.frame.width < self.view.frame.height ? self.view.frame.width : self.view.frame.height
        }
    }
    private var vmax: CGFloat {
        get{
            return self.view.frame.width > self.view.frame.height ? self.view.frame.width : self.view.frame.height
        }
    }
    /* medidas do app */
    private var letsPlayFrame: CGRect {
        get{
            return CGRectMake(0.1*vw, 0.11*vh, 0.8*vw, self.playImageSize.height*0.8*vw/self.playImageSize.width)
        }
    }
    private var letsPlayFrameHighlight: CGRect {
        get{
            return CGRectMake(0.05*vw, 0.11*vh, 0.9*vw, self.playImageSize.height*0.9*vw/self.playImageSize.width)
        }
    }
    private var collectionViewFrame: CGRect {
        get {
            return CGRectMake(0.0*vw, self.playImageSize.height*0.8*vw/self.playImageSize.width + 0.12*vh, 1*vw, vh - (self.playImageSize.height*0.8*vw/self.playImageSize.width + 0.12*vh))
        }
    }
    /* fim da Area de Design */
    
    
    private var tutorialObject: Tutorial!
    //var iniPoint: CGPoint!
    private var largeImageView: UIImageView! = nil
    private var playImageSize: CGSize!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var playImageView: UIImageView!
    @IBOutlet weak var scoreView: ScoreView!
    
    private let reuseIdentifier = "MenuKindCell"
    private let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    
    private var itens:[MenuKindItem] = [MenuKindItem]()
    
    private var directionScroll: Direction = .None
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.directionScroll = .None
        /* background */
        self.view.backgroundColor = UIColor.whiteColor()
        
        /* play image */
        var playImage: NSString = NSLocalizedString("letsplay", comment: "Imagem para ideia de play no xcassets(jogar)")
        self.playImageView.removeFromSuperview()
        var myImage = UIImage(named: playImage as String)
        self.playImageSize = myImage!.size
        var newImageView = UIImageView(frame: letsPlayFrame)
        self.view.addSubview(newImageView)
        newImageView.image = myImage
        self.playImageView = newImageView
        
        self.playImageView.layer.cornerRadius = 10
        /* scroll view */
        self.scrollView = UIMenuScroll(frame: CGRectNull)
        self.scrollView.layer.zPosition = 1
        self.scrollView.backgroundColor = UIColor.clearColor()
        //let gestureRecognizer = UITapGestureRecognizer(target: self, action: "handlePan:")
        //self.scrollView.addGestureRecognizer(gestureRecognizer)
        self.scrollView.collection = self.collectionView
        self.scrollView.orientation = 1
        self.view.addSubview(self.scrollView)
        
        /* botão de som */
        let tam = self.view.frame.width < self.view.frame.height ? 0.1 * self.view.frame.width : 0.1 * self.view.frame.height
        volumeView = UIView(frame: CGRectMake(self.view.frame.width - tam, self.view.frame.height - tam, tam, tam))
        volumeImageView = UIImageView(frame: CGRectMake(0, 0, volumeView.frame.width, volumeView.frame.height))
        if(SoundController.sharedInstance.soundEnabled){
            volumeImageView.image = UIImage(named: "speaker")
        }else{
            volumeImageView.image = UIImage(named: "speaker_mute")
        }
        //volumeView.backgroundColor = UIColor.blueColor()
        volumeView.layer.zPosition = 4
        let gestureRec = UITapGestureRecognizer(target: self, action: "toggleSound:")
        volumeView.addGestureRecognizer(gestureRec)
        self.view.addSubview(volumeView)
        volumeView.addSubview(volumeImageView)
        
        /* tutorial */
        
        tutorialObject = Tutorial(view: self.view)
        tutorialObject.setup()
        
        /* temporario, mas eh para funcionar */
        if (UIApplication.sharedApplication().delegate as! AppDelegate).isFirstTimeLanch {
            MenuServices.createMenu("Presidentes", imageMenuL: "flags", imageMenuP: "flags_p")
            
            MenuDAO.unlockPhase("Presidentes")
            
            var menuPresidentes: Menu = MenuDAO.findByDescription("Presidentes")!
            
            PhaseServices.createPhase("Brazil", dependent: "lula", tip: "He was union leader. He has had an accident at work and lost a body part.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("Brazil", dependent: "dilma", tip: "First woman to be president at her country. The presidency was his first elective office.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("Brazil", dependent: "fhc", tip: "Sociologist. He was finance minister before being elected president.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("USA", dependent: "bush", tip: "He is son of a former president, launched several military campaigns.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("USA", dependent: "obama", tip: "He was awarded the Nobel Peace Prize.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("USA", dependent: "jfk", tip: "He was assassinated while in office, has won a Pulitzer Prize.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("USA", dependent: "lincoln", tip: "He was assassinated while in office, faced a civil war.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("Germany", dependent: "merkel", tip: "First woman to be Chancellor at her country. She was named the most powerful woman in the world for several times.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("Brazil", dependent: "juscelino", tip: "He is best known for the construction of a new capital and the long-term planning.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("Israel", dependent: "netanyahu", tip: "He is the youngest Prime Minister in this asian country. He has studied in MIT.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("Russia", dependent: "putin", tip: "He was president and prime minister from one of the largest countries in the World.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("Brazil", dependent: "vargas", tip: "South american president. He has led a coup d'etat, later legally elected.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("USA", dependent: "nixon", tip: "He was the first president to resign the office in his country.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("USA", dependent: "roosevelt", tip: "Speak softly and carry a big stick.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("Spain", dependent: "zapatero", tip: "Prime minister of an iberian country.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("Spain", dependent: "felipe4", tip: "King of an iberian country.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("Japan", dependent: "shinzo", tip: "Prime minister of an island nation in East Asia.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("China", dependent: "jinping", tip: "President of one of the most populous countries in Asia." , phaseMenu: menuPresidentes)
            PhaseServices.createPhase("South_Korea", dependent: "park", tip: "She is the first female head of state in the modern history of Eastern Asia.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("Poland", dependent: "duda", tip: "President of a former socialist country in Europe. Fellow countryman of John Paul II.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("South_Africa", dependent: "zuma", tip: "His country hosted the Soccer World Cup recently.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("Mexico", dependent: "pena", tip: "President of a north american country.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("Netherlands", dependent: "rutte", tip: "Prime minister of one of the lowest countries in europe.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("Venezuela", dependent: "maduro", tip: "South american president that led his country to a deep crisis.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("Venezuela", dependent: "chavez", tip: "Socialist president, ruled for 14 years until his death from cancer.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("Bolivia", dependent: "morales", tip: "First president to come from the indigenous population.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("Bolivia", dependent: "mesa", tip: "South american dictator. He has led a coup d'etat.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("Australia", dependent: "abbott", tip: "Prime minister of the most developed country in the south hemisphere.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("Egypt", dependent: "mubarak", tip: "African president for almost thirty years until he was forced to resign.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("Iran", dependent: "ahmadinejad", tip: "Arabic president. Developed the nuclear program in his country.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("Chile", dependent: "bachele", tip: "South american president, the first woman to be elected in her country. Her country is one of the narrowest in the world.", phaseMenu: menuPresidentes)
            PhaseServices.createPhase("Italy", dependent: "berlusconi", tip: "European prime minister, owner of owner of AC Milan.", phaseMenu: menuPresidentes)
            
            menuPresidentes = MenuDAO.findByDescription("Presidentes")!
            
            println(menuPresidentes.menuPhases.count)
            /* criando novo item no menu */
            var menuNovo =  MenuServices.createMenu("Footballer", imageMenuL: "soccer_l", imageMenuP: "soccer_p")
            
            PhaseServices.createPhase("Brazil", dependent: "Ademir", tip: "His nickname which means \"jaw\".", masterDescription: "Ademir Marques de Menezes", phaseMenu: menuNovo)
            PhaseServices.createPhase("Uruguay", dependent: "Ghiggia", tip: "He Scored a goal at 79\'\' in a game against the World Cup host.", masterDescription: "Alcides Ghiggia", phaseMenu: menuNovo)
            PhaseServices.createPhase("Brazil", dependent: "Garrincha", tip: "He is also called Bent-Legged Angel in his country.", masterDescription: "Manuel Francisco dos Santos/Garrincha", phaseMenu: menuNovo)
            PhaseServices.createPhase("Brazil", dependent: "Zagallo", tip: "He was the first footballer to win the World Cup both as a manager and as a player.", masterDescription: "Mário Zagallo", phaseMenu: menuNovo)
            PhaseServices.createPhase("Brazil", dependent: "Pele", tip: "The best footballer from all the time.", masterDescription: "Pelé", phaseMenu: menuNovo)
            PhaseServices.createPhase("Brazil", dependent: "Taffarel", tip: "The town's mayor of Ozoir-la-Ferrière proposed renaming a stadium after him.", masterDescription: "Cláudio Taffarel", phaseMenu: menuNovo)
            PhaseServices.createPhase("Brazil", dependent: "Romario", tip: "He is the third highest goalscorer for his national team.", masterDescription: "Romário de Souza Faria", phaseMenu: menuNovo)
            PhaseServices.createPhase("Brazil", dependent: "Rivelino", tip: "He was famous for his large moustache.", masterDescription: "Roberto Rivellino", phaseMenu: menuNovo)
            PhaseServices.createPhase("Brazil", dependent: "Ronaldo", tip: "He is popularly dubbed \"the phenomenon\".", masterDescription: "Ronaldo Luís Nazário de Lima", phaseMenu: menuNovo)
            PhaseServices.createPhase("Brazil", dependent: "kaka", tip: "He was the first sportsperson to amass 10 million followers on Twitter.", masterDescription: "Ricardo Izecson dos Santos Leite/Kaká", phaseMenu: menuNovo)
            PhaseServices.createPhase("France", dependent: "Zidane", tip: "He gave a headbutt to his opponent in a World Cup Final", masterDescription: "Zinedine Zidane", phaseMenu: menuNovo)
            PhaseServices.createPhase("Argentina", dependent: "Messi", tip: "Often considered the best player in the world and rated by some in the sport as the greatest of all time.", masterDescription: "Lionel Messi", phaseMenu: menuNovo)
            PhaseServices.createPhase("Argentina", dependent: "Maradona", tip: "He scored an unpenalized handling foul known as the \"Hand of God\" in a World Cup.", masterDescription: "Diego Maradona", phaseMenu: menuNovo)
            PhaseServices.createPhase("Italy", dependent: "Materazzi", tip: "He received a headbutt in a World Cup Final.", masterDescription: "Marco Materazzi", phaseMenu: menuNovo)
            PhaseServices.createPhase("Portugal", dependent: "Cristiano", tip: "He became the all-time top scorer in the UEFA European Championship with 23 goals in 2014.", masterDescription: "Cristiano Ronaldo", phaseMenu: menuNovo)
            PhaseServices.createPhase("Brazil", dependent: "Neymar", tip: "At the age of 19, he won the 2011 South American Footballer of the Year award.", masterDescription: "Neymar da Silva Santos Júnior", phaseMenu: menuNovo)
            PhaseServices.createPhase("Spain", dependent: "Iniesta", tip: "He scored the only goal in the only World Cup Final that his country won.", masterDescription: "Andrés Iniesta", phaseMenu: menuNovo)
            PhaseServices.createPhase("Spain", dependent: "Ramos", tip: "He celebrated victories for both club and country by playing with a matador's cape.", masterDescription: "Sergio Ramos", phaseMenu: menuNovo)
            PhaseServices.createPhase("Italy", dependent: "Maldini", tip: "He spent all 25 seasons of his career at Serie A club A.C. Milan.", masterDescription: "Paolo Maldini", phaseMenu: menuNovo)
            PhaseServices.createPhase("Iran", dependent: "Hejazi", tip: "He is Considered as one of the best goalkeepers in the history of Asia", masterDescription: "Nasser Hejazi", phaseMenu: menuNovo)
            PhaseServices.createPhase("Italy", dependent: "Facchetti", tip: "He played for his whole career during the 1960s and 1970s, playing 634 official games and scoring 75 goals.", masterDescription: "Giacinto Facchetti", phaseMenu: menuNovo)
            PhaseServices.createPhase("Italy", dependent: "Buffon", tip: "He has been named the IFFHS goalkeeper of the year four times.", masterDescription: "Giacinto Facchetti", phaseMenu: menuNovo)
            PhaseServices.createPhase("France", dependent: "Henry", tip: "He is his country's record goalscorer", masterDescription: "Thierry Henry", phaseMenu: menuNovo)
            PhaseServices.createPhase("Côte_d'Ivoire", dependent: "Drogba", tip: "He was voted by Chelsea supporters as the club's greatest ever player.", masterDescription: "Didier Drogba", phaseMenu: menuNovo)
            PhaseServices.createPhase("Germany", dependent: "Neuer", tip: "He is considered by some to be the best goalkeeper in the world since Lev Yashin.", masterDescription: "Manuel Neuer", phaseMenu: menuNovo)
            PhaseServices.createPhase("Russia", dependent: "Lev", tip: "He is nicknamed \"The Black Spider\" or \"The Black Panther\"", masterDescription: "Lev Yashin", phaseMenu: menuNovo)
            PhaseServices.createPhase("Germany", dependent: "Klose", tip: "He is the top goalscorer in the history of the FIFA World Cup.", masterDescription: "Miroslav Klose", phaseMenu: menuNovo)
            PhaseServices.createPhase("Uruguay", dependent: "Suarez", tip: "He is also known by his bite.", masterDescription: "Luis Alberto Suárez", phaseMenu: menuNovo)
            PhaseServices.createPhase("United_Kingdom", dependent: "Rooney", tip: "He, originally a boxer, still pursues the sport recreationally for stress relief.", masterDescription: "Wayne Rooney", phaseMenu: menuNovo)
            PhaseServices.createPhase("United_Kingdom", dependent: "Beckham", tip: "He get married at Luttrellstown Castle in Ireland with a \"Spice Girls\".", masterDescription: "David Beckham", phaseMenu: menuNovo)
            
            menuNovo =  MenuServices.createMenu("Authors", imageMenuL: "authors", imageMenuP: "authors")
            PhaseServices.createPhase("jane", dependent: "pride", tip: "Is a novel of manners. Its authors is one of the most widely read writers in English literature", masterDescription: "Pride and Prejudice", phaseMenu: menuNovo)
            PhaseServices.createPhase("honore", dependent: "goriot", tip: "Its author is a French novelist and playwright.", masterDescription: "Father Goriot", phaseMenu: menuNovo)
            PhaseServices.createPhase("lewis", dependent: "alice", tip: "It is considered to be one of the best examples of the literary nonsense genre.", masterDescription: "Alice's Adventures in Wonderland", phaseMenu: menuNovo)
            PhaseServices.createPhase("dickens", dependent: "twocities", tip: "The novel is set in London and Paris before and during the French Revolution. Its authors is regarded as the greatest novelist of the Victorian era.", masterDescription: "A Tale of Two Cities", phaseMenu: menuNovo)
            PhaseServices.createPhase("dickens", dependent: "david", tip: "The original title was the Personal History, Adventures, Experience and Observation of David Copperfield the Younger of Blunderstone Rookery (which he never meant to publish on any account). Its authors is regarded as the greatest novelist of the Victorian era.", masterDescription: "David Copperfield", phaseMenu: menuNovo)
            PhaseServices.createPhase("doyle", dependent: "sherlock", tip: "It tells the story of a London-based \"consulting detective\" whose abilities border on the fantastic. Its authors was appointed a Knight of Grace of the Order of the Hospital of Saint John of Jerusalem.", masterDescription: "The Memoirs of Sherlock Holmes", phaseMenu: menuNovo)
            PhaseServices.createPhase("doyle", dependent: "lost", tip: "It tells the story of an expedition to a plateau in the Amazon basin of South America where prehistoric animals (dinosaurs and other extinct creatures) still survive. Its authors was appointed a Knight of Grace of the Order of the Hospital of Saint John of Jerusalem.", masterDescription: "The Lost World", phaseMenu: menuNovo)
            PhaseServices.createPhase("fyodor", dependent: "crime", tip: "Its authors literary works explore human psychology in the troubled political, social, and spiritual atmosphere of 19th-century Russia.", masterDescription: "Crime and Punishment", phaseMenu: menuNovo)
            PhaseServices.createPhase("fyodor", dependent: "brothers", tip: "It is a spiritual drama of moral struggles concerning faith, doubt, and reason. Its authors literary works explore human psychology in the troubled political, social, and spiritual atmosphere of 19th-century Russia.", masterDescription: "The Brothers Karamazov", phaseMenu: menuNovo)
            PhaseServices.createPhase("goethe", dependent: "faust", tip: "It's its author's magnum opus and considered by many to be one of the greatest works of German literature.", masterDescription: "Faust", phaseMenu: menuNovo)
            PhaseServices.createPhase("goethe", dependent: "werther", tip: "It is an epistolary and loosely autobiographical novel. It reputedly also led to some of the first known examples of copycat suicide.", masterDescription: "The Sorrows of Young Werther", phaseMenu: menuNovo)
            PhaseServices.createPhase("grimm", dependent: "snow", tip: "It features such elements as the magic mirror, the poisoned apple, the glass coffin, and the characters of the evil queen/stepmother and the seven dwarfs. Its is a german fairy tale, first recorded by two brothers.", masterDescription: "Snow White", phaseMenu: menuNovo)
            PhaseServices.createPhase("grimm", dependent: "hansel", tip: "Hansel and Gretel are a young brother and sister threatened by a cannibalistic witch living in the forest in a house of candy. Its is a german fairy tale, first recorded by two brothers.", masterDescription: "Hansel and Gretel", phaseMenu: menuNovo)
            PhaseServices.createPhase("homer", dependent: "iliada", tip: "It is an ancient Greek epic poem, tells the Trojan War story.", masterDescription: "The Iliad", phaseMenu: menuNovo)
            PhaseServices.createPhase("homer", dependent: "odyssey", tip: "It is one of two major ancient Greek epic poems. It is, in part, a sequel to the Iliad.", masterDescription: "The Odyssey", phaseMenu: menuNovo)
            PhaseServices.createPhase("lovecraft", dependent: "cthulhu", tip: "It introduces Cthulhu, a gigantic entity worshiped by cultists. Some its authors' work was inspired by his own nightmares.", masterDescription: "The Call of Cthulhu", phaseMenu: menuNovo)
            PhaseServices.createPhase("shakespeare", dependent: "hamlet", tip: "Its full name is 'The Tragedy of Hamlet, Prince of Denmark'. It is its author longest play and among the most powerful and influential tragedies in english literature.", masterDescription: "Hamlet", phaseMenu: menuNovo)
            PhaseServices.createPhase("shakespeare", dependent: "romeo", tip: "One of the most performed plays around the world. The title characters are regarded as archetypal young lovers. Classic of the english literature.", masterDescription: "Romeo and Juliet", phaseMenu: menuNovo)
            PhaseServices.createPhase("shelley", dependent: "frankenstein", tip: "Its author wrote it when she was 19 years old. It is considered the earliest science fiction novel and also one of the greatest horror books.", masterDescription: "Frankenstein: or the Modern Prometheus", phaseMenu: menuNovo)
            PhaseServices.createPhase("verne", dependent: "twenty", tip: "The description of Captain Nemo's ship, the Nautilus, was considered ahead of its time, as it accurately describes features on submarines, with scientifical accuracy.", masterDescription: "Twenty Thousand Leagues Under the Sea", phaseMenu: menuNovo)
            PhaseServices.createPhase("bram", dependent: "dracula", tip: "Famous for introducing the character of the vampire Count Dracula. It is not the first vampire's story, but defined its modern form.", masterDescription: "Dracula", phaseMenu: menuNovo)
            PhaseServices.createPhase("wells", dependent: "war", tip: "It is a science fiction novel, that narrates the invasion of the Earth by the martians. In 1938 a radio program read this book as it was ocurring a real invasion and caused mass histeria.", masterDescription: "The War of the Worlds", phaseMenu: menuNovo)
            PhaseServices.createPhase("machado", dependent: "dom", tip: "Its authors is the founder of the Academy of Letters of his country and is considered they're greatest writer.", masterDescription: "Lord Taciturn", phaseMenu: menuNovo)
            PhaseServices.createPhase("machado", dependent: "bras", tip: "Its authors is the founder of the Academy of Letters of his country and is considered they're greatest writer.", masterDescription: "The Posthumous Memoirs of Bras Cubas", phaseMenu: menuNovo)

            /* criando novo item no menu */
            menuNovo =  MenuServices.createMenu("Filmes", imageMenuL: "filmes", imageMenuP: "filmes")
            
            PhaseServices.createPhase("batman_filme", dependent: "coringa", tip: "O mais famoso vilão da série de filmes que participa", masterDescription: "Batman", phaseMenu: menuNovo)
            PhaseServices.createPhase("batman_filme", dependent: "bruce_wayne", tip: "Super heroi bilionario americano", masterDescription: "Batman", phaseMenu: menuNovo)
            PhaseServices.createPhase("batman_filme", dependent: "lucius_fox", tip: "CEO da empresa de um super-heroi", masterDescription: "Batman", phaseMenu: menuNovo)
            PhaseServices.createPhase("batman_filme", dependent: "jim_gordon", tip: "Comissario de policia aliada a super-heroi", masterDescription: "Batman", phaseMenu: menuNovo)
            PhaseServices.createPhase("batman_filme", dependent: "Rachel_Dowes", tip: "Namorada de infancia de super-heroi", masterDescription: "Batman", phaseMenu: menuNovo)
            PhaseServices.createPhase("terminator_filme", dependent: "terminator", tip: "Da nome ao filme que participou", masterDescription: "The Terminator", phaseMenu: menuNovo)
            PhaseServices.createPhase("terminator_filme", dependent: "sarah_connor", tip: "Mãe do líder da resistência humana na guerra contra as máquinas", masterDescription: "The Terminator", phaseMenu: menuNovo)
            PhaseServices.createPhase("terminator_filme", dependent: "kyle_reese", tip: "Pai do líder da resistência humana na guerra contra as máquinas", masterDescription: "The Terminator", phaseMenu: menuNovo)
            PhaseServices.createPhase("terminator_filme", dependent: "john_connor", tip: "Líder da resistência humana na guerra contra as máquinas", masterDescription: "The Terminator", phaseMenu: menuNovo)
            PhaseServices.createPhase("o_lobo_filme", dependent: "jordan_belfort", tip: "Corretor de ações corrupto protagonista do filme", masterDescription: "O Lobo de Wall Street", phaseMenu: menuNovo)
            PhaseServices.createPhase("o_lobo_filme", dependent: "donnie_zoff", tip: "Corretor de ações corrupto socio do protagonista do filme", masterDescription: "O Lobo de Wall Street", phaseMenu: menuNovo)
            PhaseServices.createPhase("o_lobo_filme", dependent: "naomi_lapaglia", tip: "Segunda esposa do corretor de ações corrupto protagonista do filme", masterDescription: "O Lobo de Wall Street", phaseMenu: menuNovo)
            PhaseServices.createPhase("branquelas_filme", dependent: "kevin_copeland", tip: "Protagonista que passa  amior parte do filme disfarçado", masterDescription: "As Branquelas", phaseMenu: menuNovo)
            PhaseServices.createPhase("branquelas_filme", dependent: "latrell", tip: "Sua dublagem de Thousand Miles foi um dos principais destaques do filme", masterDescription: "As Branquelas", phaseMenu: menuNovo)
            PhaseServices.createPhase("superman", dependent: "general_zod", tip: "Alienigena que é o principal vilão do filme", masterDescription: "O Homem de Aço", phaseMenu: menuNovo)
            PhaseServices.createPhase("superman", dependent: "perry_white", tip: "O editor-chefe do Planeta Diário", masterDescription: "O Homem de Aço", phaseMenu: menuNovo)
            PhaseServices.createPhase("superman", dependent: "lois_lane", tip: "Repórter do Planeta Diário e interesse amoroso de protagonista", masterDescription: "O Homem de Aço", phaseMenu: menuNovo)
            PhaseServices.createPhase("superman", dependent: "clark_kent", tip: "Superheroi protagonista disfarçado", masterDescription: "O Homem de Aço", phaseMenu: menuNovo)
            PhaseServices.createPhase("interstellar", dependent: "amelia_brand", tip: "Cientista e Astronauta é uma das protagonistas de seu filme", masterDescription: "Interestelar", phaseMenu: menuNovo)
            PhaseServices.createPhase("interstellar", dependent: "joseph_cooper", tip: "Astronauta é o protagonista de seu filme", masterDescription: "Interestelar", phaseMenu: menuNovo)
            PhaseServices.createPhase("transcendence", dependent: "evelyn_caster", tip: "Esposa do protagonista e principal responsavel por seu fracasso", masterDescription: "Transcendence", phaseMenu: menuNovo)
            PhaseServices.createPhase("transcendence", dependent: "joseph_tagger", tip: "Agente do FBI e amigo da esposa do protagonista", masterDescription: "Transcendence", phaseMenu: menuNovo)
            PhaseServices.createPhase("back_future", dependent: "marty_mcfly", tip: "Protagonista do filme nascido  em 1968 em Hill Valley", masterDescription: "De Volta Para o Futuro", phaseMenu: menuNovo)
            PhaseServices.createPhase("back_future", dependent: "doc_brown", tip: "O inventor da maquina do tempo", masterDescription: "De Volta Para o Futuro", phaseMenu: menuNovo)
            PhaseServices.createPhase("back_future", dependent: "biff_tannen", tip: "Criado pela avó é o principal vilão do filme", masterDescription: "De Volta Para o Futuro", phaseMenu: menuNovo)
            PhaseServices.createPhase("back_future", dependent: "biff_tannen_2", tip: "Criado pela avó é o principal vilão do filme", masterDescription: "De Volta Para o Futuro", phaseMenu: menuNovo)
            PhaseServices.createPhase("iron_man_filme", dependent: "tony_stark", tip: "Super heroi bilionario americano", masterDescription: "O Homem de Ferro", phaseMenu: menuNovo)
            PhaseServices.createPhase("iron_man_filme", dependent: "james_rhodes", tip: "Coronel amigo do protagonista. Faz a ligação entre a empresa do protagonista e a força area dos EUA", masterDescription: "O Homem de Ferro", phaseMenu: menuNovo)
            PhaseServices.createPhase("iron_man_filme", dependent: "pepper", tip: "Assistente pessoal do protagonista e interesse amoroso do mesmo", masterDescription: "O Homem de Ferro", phaseMenu: menuNovo)
            PhaseServices.createPhase("iron_man_filme", dependent: "agente_coulson", tip: "Agente da S.H.I.E.L.D", masterDescription: "O Homem de Ferro", phaseMenu: menuNovo)
            
            /* criando novo item no menu */
            menuNovo =  MenuServices.createMenu("TV Series", imageMenuL: "tv_l", imageMenuP: "tv_l")
            
            PhaseServices.createPhase("chuck_tv", dependent: "Yvonne", tip: "She gets her memories suppressed due to a faulty Intersect upload.", masterDescription: "Sarah Lisa Bartowski/Yvonne Strahovski", phaseMenu: menuNovo)
            PhaseServices.createPhase("chuck_tv", dependent: "chuck", tip: "He works at a dead end job at the Burbank Buy More in its Nerd Herd division.", masterDescription: "Charles Irving Bartowski/Chuck Bartowski/Zachary Levi", phaseMenu: menuNovo)
            PhaseServices.createPhase("chuck_tv", dependent: "morgan", tip: "He best friend opens an email that causes to download into his brain the full contents of a CIA/NSA supercomputer known as the Intersect, which has served as a database for their combined collected intelligence.", masterDescription: "Morgan Guillermo Grimes/Joshua Gomez", phaseMenu: menuNovo)
            PhaseServices.createPhase("chuck_tv", dependent: "casey", tip: "He was born by the name Alexander Coburn, but he had to change to become an agent for the NSA. He was also known as \"Angel of Death\" 'cause a mission in the communist country Costa Gavras, but nowadays he is\"Angel of Live\".", masterDescription: "John Casey/Adam Baldwin", phaseMenu: menuNovo)
            PhaseServices.createPhase("chuck_tv", dependent: "ellie", tip: "The tv show that her appears has her brother's name.", masterDescription: "Ellie Bartowski Woodcomb/Sarah Lancaster", phaseMenu: menuNovo)
            PhaseServices.createPhase("Arrow_tv", dependent: "felicity", tip: "She is especialisty on computers and already date two superheroes.", masterDescription: "Felicity Smoak/Emily Bett Rickards", phaseMenu: menuNovo)
            PhaseServices.createPhase("Arrow_tv", dependent: "oliver", tip: "For five years he stayed on a hellish island.", masterDescription: "Oliver Queen/Stephen Amell", phaseMenu: menuNovo)
            PhaseServices.createPhase("Arrow_tv", dependent: "diggle", tip: "He is a bodyguard, ex-soldier and a member of a superheroes team.", masterDescription: "John Diggle/David Ramsey", phaseMenu: menuNovo)
            PhaseServices.createPhase("Arrow_tv", dependent: "roy", tip: "He is/was a vigilante partner with the codename Arsenal.", masterDescription: "Roy Harper/Colton Haynes", phaseMenu: menuNovo)
            PhaseServices.createPhase("Arrow_tv", dependent: "thea", tip: "Her brother stayed on a hellish island for five years.", masterDescription: "Thea Queen/Willa Holland", phaseMenu: menuNovo)
            PhaseServices.createPhase("Supernatural_tv", dependent: "sam", tip: "He was possessed by the fallen arcanjo Lucifer.", masterDescription: "Sam Winchester/Jared Padalecki", phaseMenu: menuNovo)
            PhaseServices.createPhase("Supernatural_tv", dependent: "dean", tip: "He gets the mark of Cain and became a specie of demon.", masterDescription: "Dean Winchester/Jensen Ackles", phaseMenu: menuNovo)
            PhaseServices.createPhase("Supernatural_tv", dependent: "castiel", tip: "He is an angel.", masterDescription: "Castiel/Misha Collins", phaseMenu: menuNovo)
            PhaseServices.createPhase("Supernatural_tv", dependent: "crowley", tip: "He is a demon.", masterDescription: "Crowley/Mark Sheppard", phaseMenu: menuNovo)
            PhaseServices.createPhase("Supernatural_tv", dependent: "bobby", tip: "He is an old friend of John Winchester, and over time evolved into a father-figure for John's sons.", masterDescription: "Bobby Singer/Jim Beaver", phaseMenu: menuNovo)
            PhaseServices.createPhase("penny_tv", dependent: "ives", tip: "She is an expert medium and was confused as a crazy person.", masterDescription: "Vanessa Ives/Eva Green", phaseMenu: menuNovo)
            PhaseServices.createPhase("penny_tv", dependent: "murray", tip: "He played a Bram Stoker book character.", masterDescription: "Sir Malcolm Murray/Timothy Dalton", phaseMenu: menuNovo)
            PhaseServices.createPhase("penny_tv", dependent: "victor", tip: "He played the protagonist of a Mary Shelley's novel.", masterDescription: "Victor Frankenstein/Harry Treadaway", phaseMenu: menuNovo)
            PhaseServices.createPhase("penny_tv", dependent: "gray", tip: "He played the protagonist of a Oscar Wilde's novel.", masterDescription: "Dorian Gray/Reeve Carney", phaseMenu: menuNovo)
            PhaseServices.createPhase("penny_tv", dependent: "ethan", tip: "He is an American sharpshooter running away from a checkered past.", masterDescription: "Ethan Chandler/Josh Hartnett", phaseMenu: menuNovo)
            PhaseServices.createPhase("tbbt_tv", dependent: "Leonard", tip: "He is an experimental physicist.", masterDescription: "Leonard Hofstadter/Johnny Galecki", phaseMenu: menuNovo)
            PhaseServices.createPhase("tbbt_tv", dependent: "cooper", tip: "He is a theoretical physicist.", masterDescription: "Sheldon Lee Cooper/Jim Parsons", phaseMenu: menuNovo)
            PhaseServices.createPhase("tbbt_tv", dependent: "Howard", tip: "He is a Jewish aerospace engineer and astronaut.", masterDescription: "Howard Wolowitz/Simon Helberg", phaseMenu: menuNovo)
            PhaseServices.createPhase("tbbt_tv", dependent: "Raj", tip: "He is an indian astrophysicist.", masterDescription: "Rajesh Koothrappali/Kunal Nayyar", phaseMenu: menuNovo)
            PhaseServices.createPhase("tbbt_tv", dependent: "Penny", tip: "She is a unsuccessful actress that was born on a farm near Omaha, Nebraska.", masterDescription: "Penny/Kaley Cuoco-Sweeting", phaseMenu: menuNovo)
            PhaseServices.createPhase("americans_tv", dependent: "elizabeth", tip: "She is a Russian spy infiltrated in USA.", masterDescription: "Elizabeth Jennings/Keri Russell", phaseMenu: menuNovo)
            PhaseServices.createPhase("americans_tv", dependent: "philip", tip: "He is a Russian spy infiltrated in USA.", masterDescription: "Philip Jennings/Matthew Rhys", phaseMenu: menuNovo)
            PhaseServices.createPhase("got_tv", dependent: "Daenerys", tip: "She is introduced as Daenerys Stormborn of the House Targaryen, the First of Her Name, the Unburnt, Queen of Meereen, Queen of the Andals and the Rhoynar and the First Men, Khaleesi of the Great Grass Sea, Breaker of Chains, and Mother of Dragons.", masterDescription: "Daenerys Targaryen/Emilia Clarke", phaseMenu: menuNovo)
            PhaseServices.createPhase("got_tv", dependent: "Melisandre", tip: "She is a priestess of the Lord of Light and a close advisor to Stannis Baratheon in his campaign to take the Iron Throne.", masterDescription: "Melisandre/Carice van Houten", phaseMenu: menuNovo)
            PhaseServices.createPhase("got_tv", dependent: "Eddard", tip: "He is the head of House Stark, the Lord of Winterfell, Lord Paramount and Warden of the North, and Hand of the King to Robert I Baratheon.", masterDescription: "Eddard Stark/Sean Bean", phaseMenu: menuNovo)
            PhaseServices.createPhase("gossip_tv", dependent: "Dan", tip: "He reveals himself to be an anonymous blogger.", masterDescription: "Dan Humphrey/Penn Badgley", phaseMenu: menuNovo)
            PhaseServices.createPhase("gossip_tv", dependent: "Blair", tip: "She said: \"There's only one queen b**** in this town and that's me\".", masterDescription: "Blair Waldorf/Leighton Meester", phaseMenu: menuNovo)
            PhaseServices.createPhase("malcon_tv", dependent: "Malcolm", tip: "He said: \"You want to know the best part about childhood? At some point, it ends.\".", masterDescription: "Malcolm Wilkerson/Frankie Muniz", phaseMenu: menuNovo)
            PhaseServices.createPhase("malcon_tv", dependent: "Lois", tip: "She had five kids and is known by her hot temper and controlling behavior.", masterDescription: "Lois Wilkerson/Jane Kaczmarek", phaseMenu: menuNovo)
            PhaseServices.createPhase("bones_tv", dependent: "temperance", tip: "She is a forensic anthropologist and works at the Jeffersonian Institute in Washington, D.C.", masterDescription: "Temperance Brennan/Emily Deschanel", phaseMenu: menuNovo)
            PhaseServices.createPhase("bones_tv", dependent: "booth", tip: "He is a Special Agent with the FBI and the current FBI liaison to the Jeffersonian.", masterDescription: "Seeley Booth/David Boreanaz", phaseMenu: menuNovo)
        }

        for menu in MenuDAO.getAllMenu() {
            self.itens.insert( MenuKindItem(menu: menu), atIndex: 0)
            //println(menu.descriptionMenu + " " + menu.menuPhases.count.description)
        }
    }
    
    func photoForIndexPath(indexPath: NSIndexPath) -> MenuKindItem {
        return itens[indexPath.row]
    }
    
    func toggleSound(gestureRecognizer: UITapGestureRecognizer){
        if(SoundController.sharedInstance.soundEnabled){
            SoundController.sharedInstance.soundEnabled = false
            volumeImageView.image = UIImage(named: "speaker_mute")
        }else{
            SoundController.sharedInstance.soundEnabled = true
            volumeImageView.image = UIImage(named: "speaker")
        }
    }
    
    /*func handlePan(gestureRecognizer: UITapGestureRecognizer) {
        //println(vel)
        let pos = gestureRecognizer.locationInView(self.collectionView)
        print("Tap: ")
        println(pos)
        var ip = self.collectionView.indexPathForItemAtPoint(pos)
        if( ip != nil){
            self.collectionView.selectItemAtIndexPath(ip, animated: true, scrollPosition: nil)
        }else{
        }
    }*/
    
    
    func scrollAutomaticALitBit(){
        if self.directionScroll != .None{
            
            let newYOffset = self.collectionView.contentOffset.y + (CGFloat(self.directionScroll.rawValue) * self.velocityAutoScroll)
            
            if (newYOffset < self.collectionView.contentSize.height - self.collectionView.frame.size.height)&&( newYOffset > 0 ){
                self.collectionView.setContentOffset(CGPoint(x: self.collectionView.contentOffset.x, y: newYOffset ), animated: true)
            }
        }
    }
    
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.originFrame =
            selectedImage!.superview!.convertRect(selectedImage!.frame, toView: nil)
        
        transition.presenting = true
        
        return transition
    }
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        //reverseTransition.presenting = false
        transition.presenting = true
        return transition
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        self.collectionView.reloadData()
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        /* design de fundo */
        if((fundoView) != nil){
            fundoView.removeFromSuperview()
        }
        
        fundoView = UIImageView(frame: self.view.frame)
        fundoView.layer.zPosition = -1
        fundoView.backgroundColor = UIColor.whiteColor()
        var gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = view.bounds
        gradient.colors = [UIColor.whiteColor().CGColor,UIColor.blueColor().CGColor]
        //gradient.locations = [0.0, 0.03, 0.97, 1.0];
        gradient.startPoint = CGPointMake(0, 0)
        gradient.endPoint = CGPointMake(1, 1)
        gradient.opacity = 0.5
        fundoView.layer.insertSublayer(gradient, atIndex: 0)
        gradient = CAGradientLayer()
        gradient.frame = view.bounds
        gradient.colors = [UIColor.whiteColor().CGColor,UIColor.redColor().CGColor]
        gradient.startPoint = CGPointMake(1, 0)
        gradient.endPoint = CGPointMake(0, 1)
        gradient.opacity = 0.5
        fundoView.layer.insertSublayer(gradient, atIndex: 0)
        self.view.addSubview(fundoView)
        //fundoView.alpha = 0.5
        
        // posicao do letsplay
        //self.playImageView.frame.origin.x = (self.view.frame.width-self.playImageView.frame.width)/2

        self.playImageView.frame = letsPlayFrame
        self.collectionView.frame = collectionViewFrame
        
        //self.scrollView.frame = self.collectionView.frame
        
        /* botao som */
        let tam = self.view.frame.width < self.view.frame.height ? 0.1 * self.view.frame.width : 0.1 * self.view.frame.height
        volumeView.frame = CGRectMake( self.view.frame.width - tam, self.view.frame.height - tam, tam, tam)
        volumeImageView.frame = CGRectMake(0, 0, volumeView.frame.width, volumeView.frame.height)
        
        tutorialObject.reshape()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.scrollView.frame = self.collectionView.frame
    }
}

extension MenuKindVC : UICollectionViewDataSource {
    
    /* numero de itens no menu */
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    /* numero de imagens por menu */
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itens.count
    }
    
    /* configurar as celulas */
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! MenuKindCell
        // Configure the cell
        let menuPhoto = photoForIndexPath(indexPath)

        cell.setDelegateTouch(delegateTouch: self)
        cell.myselfKindItem = menuPhoto
        
        cell.imageView.image = menuPhoto.thumbnail
        cell.label.text = NSLocalizedString(menuPhoto.description, comment: "")
        
        cell.layer.cornerRadius = 10
        cell.layer.borderColor = UIColor.purpleColor().CGColor
        cell.layer.borderWidth = 1
        
        return cell
    }
}

extension MenuKindVC : UICollectionViewDelegateFlowLayout {
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            
            let itemPhoto =  photoForIndexPath(indexPath)
            
            if var image = itemPhoto.thumbnail {
                var size = self.view.frame.size
                size.width  *= 0.22
                size.height *= 0.22
                size.height += 55 /* 50 do label e 5 do espacamento entre eles*/
                return size
            }
            return CGSize(width: 100, height: 100)
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets {
            return sectionInsets
    }
    
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! MenuKindCell
        
        if cell.myselfKindItem.isLocked(){
            let cancellMessage = NSLocalizedString("Cancelar", comment: "Opcao de nao gastar dinheiro para uma ajuda")
            let okMessage = NSLocalizedString("OK", comment: "Opcao para continuar e tentar gastar dinheiro para uma ajuda")
            let message = NSLocalizedString("São necessários %d créditos.", comment: "Opcao de nao gastar dinheiro para uma ajuda")
            let titleMessage = NSLocalizedString("Gasto para liberar fase.", comment: "Titulo para alerta que confirma a necessidade de gastar credito")
            
            let custHelp: Int = PlayerDAO.calculatePayPhase()
            let refreshAlert = UIAlertController(title: titleMessage, message: NSString(format: message, custHelp) as String, preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: cancellMessage, style: .Default, handler: { (action: UIAlertAction!) in
                return
            }))
            
            refreshAlert.addAction(UIAlertAction(title: okMessage, style: .Default, handler: { (action: UIAlertAction!) in
                let itsPossible = PlayerDAO.releasePhasePay(cell.myselfKindItem.description)
                if itsPossible || PlayerDAO.requestMoreCredit(custHelp){
                    cell.myselfKindItem.menu.locked = NSNumber(bool: false)
                    self.collectionView.reloadData()
                }
            }))
            presentViewController(refreshAlert, animated: true, completion: nil)
            
        }else{
            // cria imagem
            if self.largeImageView == nil{
                self.largeImageView = UIImageView()
                self.view.addSubview(self.largeImageView)
            }
            let menuPhoto = (cell as MenuKindCell).myselfKindItem

            self.largeImageView.image = menuPhoto.thumbnail
            self.selectedImage = self.largeImageView
            self.largeImageView.frame.size = CGSize(width: self.view.frame.size.width*0.2, height: self.view.frame.size.height*0.2)
            
            self.largeImageView.layer.position = self.view.convertPoint(cell.layer.position, fromCoordinateSpace: self.collectionView)
            
            self.largeImageView.hidden = false
            // move imagem
            UIView.animateWithDuration(1.0, animations: { () -> Void in
                self.largeImageView.layer.position = self.playImageView.layer.position
            }, completion: { (result) -> Void in
                UIView.animateWithDuration(0.0, animations: { () -> Void in
                    //self.largeImageView.frame.size = CGSizeMake(self.view.frame.size.width*0.01, self.view.frame.size.height*0.01)
                    //self.largeImageView.layer.position = self.playImageView.layer.position

                }, completion: { (result) -> Void in
                    let myCell: MenuKindCell = cell as MenuKindCell
                    
                    let gameVC: GameViewController! = self.storyboard?.instantiateViewControllerWithIdentifier("GameViewController") as! GameViewController
                    gameVC.phaseName = myCell.myselfKindItem.menu
                    
                    gameVC.transitioningDelegate = self
                    self.presentViewController(gameVC, animated: true, completion: nil)
                    self.largeImageView.hidden = true
                })
            })
            // some imagem
        }
    }
}

extension MenuKindVC : UIScrollViewDelegate {
    func scrollViewDidScroll(scrollView: UIScrollView){
        self.scrollAutomaticALitBit()
    }
}

extension MenuKindVC : CollectionCellDelegate {
    func touchBegan(point: CGPoint, withCell cell: UICollectionViewCell){
        let menuPhoto = (cell as! MenuKindCell).myselfKindItem
        if self.largeImageView == nil{
            self.largeImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 0.1, height: 0.1))
            self.view.addSubview(self.largeImageView)
        }
        
        if menuPhoto.isLocked() {
            return
        }
        
        self.largeImageView.frame.size = CGSize(width: self.view.frame.size.width*0.2, height: self.view.frame.size.height*0.2)
        self.largeImageView.layer.position = point
        self.largeImageView.image = menuPhoto.thumbnail
        
        self.largeImageView.hidden = false
        
        self.selectedImage = self.largeImageView
        
        //self.playImageView.frame = CGRectMake(0.05*self.view.frame.width, 0.1*self.view.frame.height, 0.9*self.view.frame.width, self.playImageSize.height*0.9*self.view.frame.width/self.playImageSize.width)
        
        self.collectionView.scrollEnabled = false
        /* para o scroll */
        self.directionScroll = .None
    }
    
    func touchMoved(point: CGPoint, withCell cell: UICollectionViewCell){
        /* para o scroll */
        self.directionScroll = .None
        let menuPhoto = (cell as! MenuKindCell).myselfKindItem
        if menuPhoto.isLocked() {
            return
        }
        if !self.largeImageView.hidden {
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.playImageView.frame = self.letsPlayFrameHighlight
            })

            //println(NSStringFromCGPoint(newPosition))
            
            if point.x != 0 && point.y != 0 {
                self.largeImageView.layer.position = point
            }
            
            if self.playImageView.frame.contains(point) {
                self.playImageView.layer.borderColor = UIColor.redColor().CGColor
                self.playImageView.layer.borderWidth = 2.0
                self.playImageView.layer.cornerRadius = 10
            }else{
                self.playImageView.layer.borderColor = UIColor.clearColor().CGColor
                self.playImageView.layer.borderWidth = 0.0
                self.playImageView.layer.cornerRadius = 0
            }
            
            if self.largeImageView.frame.origin.y + self.largeImageView.frame.size.height > self.collectionView.frame.origin.y + self.collectionView.frame.size.height {
                /* para baixo */
                self.directionScroll = .Down
                
                self.scrollAutomaticALitBit()
            }else if (self.collectionView.frame.origin.y - self.largeImageView.frame.origin.y>50)&&(self.collectionView.frame.origin.y - self.largeImageView.frame.origin.y < 100) {
                self.directionScroll = .Up
                
                self.scrollAutomaticALitBit()
            }
        }
    }
    
    func touchEnded(point: CGPoint, withCell cell: UICollectionViewCell){
        /* para o scroll */
        self.directionScroll = .None
        let menuPhoto = (cell as! MenuKindCell).myselfKindItem
        if menuPhoto.isLocked() {
            return
        }
        if !self.largeImageView.hidden {
            if self.playImageView.frame.contains(point) {
                //present details view controller
                
                let myCell: MenuKindCell = cell as! MenuKindCell
                
                let gameVC: GameViewController! = self.storyboard?.instantiateViewControllerWithIdentifier("GameViewController") as! GameViewController
                gameVC.phaseName = myCell.myselfKindItem.menu
                
                gameVC.transitioningDelegate = self
                presentViewController(gameVC, animated: true, completion: nil)
                self.largeImageView.hidden = true
            }else{
                let size: CGSize = self.largeImageView.frame.size
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    var theFrame = self.largeImageView.frame
                    theFrame.origin = self.collectionView.convertPoint(cell.center, toView: self.largeImageView.superview)
                    theFrame.size = CGSize(width: 1, height: 1)
                    self.largeImageView.frame = theFrame
                    }, completion: { (result) -> Void in
                        self.largeImageView.hidden = true
                        self.largeImageView.frame.size = size
                })
            }
            self.collectionView.scrollEnabled = true
        }
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.playImageView.frame = self.letsPlayFrame
            self.playImageView.layer.borderWidth = 0
        })
        
    }
}




