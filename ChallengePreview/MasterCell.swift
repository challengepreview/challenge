//
//  MasterCell.swift
//  ChallengePreview
//
//  Created by Erick Ricardo Mattos on 06/07/15.
//  Copyright (c) 2015 Erick Ricardo Mattos. All rights reserved.
//

import  UIKit

class MasterCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    var myself: MasterCellItem!
}
