//
//  MenuKindItem.swift
//  ChallengePreview
//
//  Created by Erick Ricardo Mattos on 06/07/15.
//  Copyright (c) 2015 Erick Ricardo Mattos. All rights reserved.
//

import UIKit
import Foundation

extension UIImage {
    
    private func convertToGrayScaleNoAlpha() -> CGImageRef {
        let colorSpace = CGColorSpaceCreateDeviceGray();
        let bitmapInfo = CGBitmapInfo(CGImageAlphaInfo.None.rawValue)
        let context = CGBitmapContextCreate(nil, Int(size.width), Int(size.height), 8, 0, colorSpace, bitmapInfo)
        CGContextDrawImage(context, CGRectMake(0, 0, size.width, size.height), self.CGImage)
        return CGBitmapContextCreateImage(context)
    }
    
    
    /**
    Return a new image in shades of gray + alpha
    */
    func convertToGrayScale() -> UIImage {
        let bitmapInfo = CGBitmapInfo(CGImageAlphaInfo.Only.rawValue)
        let context = CGBitmapContextCreate(nil, Int(size.width), Int(size.height), 8, 0, nil, bitmapInfo)
        CGContextDrawImage(context, CGRectMake(0, 0, size.width, size.height), self.CGImage);
        let mask = CGBitmapContextCreateImage(context)
        return UIImage(CGImage: CGImageCreateWithMask(convertToGrayScaleNoAlpha(), mask), scale: scale, orientation:imageOrientation)!
    }
}

class MenuKindItem{
    private let iGL: UIImage!
    private let iGP: UIImage!
    private let iNL: UIImage!
    private let iNP: UIImage!
    var description     : String{
        get{
            return self.menu.descriptionMenu
        }
    }
    var thumbnail       : UIImage!{
        get{
            var image: UIImage!
            if UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation){
                image = UIImage(named: self.menu.imageMenuL)
            }
            image = UIImage(named: self.menu.imageMenuP)
            
            if self.menu.locked.boolValue {
                if UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation){
                    return self.iGL
                }
                return self.iGP
            }
            else{
                if UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation){
                    return self.iNL
                }
                return self.iNP
            }
        }
    }
    var menu    : Menu!
    
    init(menu: Menu){
        self.menu = menu
        self.iNL  = UIImage(named: self.menu.imageMenuL)
        self.iNP  = UIImage(named: self.menu.imageMenuP)
        self.iGL  = self.iNL.convertToGrayScale()
        self.iGP  = self.iNP.convertToGrayScale()
    }
    
    func isLocked() -> Bool{
        return self.menu.locked.boolValue
    }
}
