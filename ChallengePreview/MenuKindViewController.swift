//
//  MenuKindViewController.swift
//  ChallengePreview
//
//  Created by Erick Ricardo Mattos on 30/06/15.
//  Copyright (c) 2015 Erick Ricardo Mattos. All rights reserved.
//

import UIKit

class MenuKindViewController: UIViewController {
    private let reuseIdentifier = "MenuKindCell"
    private let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    
    private var itens = [MenuKindItem]()
    
    func photoForIndexPath(indexPath: NSIndexPath) -> MenuPhoto {
        return itens[indexPath.row].kindImage
    }
}

extension MenuKindViewController : UICollectionViewDataSource {
    
    /* numero de itens no menu */
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    /* numero de imagens por menu */
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itens.count
    }
    
    /* configurar as celulas */
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! MenuKindCell
        cell.backgroundColor = UIColor.blackColor()
        // Configure the cell
        let menuPhoto = photoForIndexPath(indexPath)
        cell.backgroundColor = UIColor.greenColor()
        
        cell.imageView.image = menuPhoto.thumbnail
        cell.label.text = ""
        
        cell.layer.cornerRadius = cell.frame.size.width/2
        
        return cell
    }
}

extension MenuKindViewController : UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        let aux = MenuKindItem(kindTerm: textField.text, kindImage: MenuPhoto(photoID: "PaisesB"))
        
        itens.insert(aux, atIndex: 0)
        
        //self.collectionView?.reloadData()
        
        textField.text = nil
        textField.resignFirstResponder()
        return true
    }
}

extension MenuKindViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            
            let itemPhoto =  photoForIndexPath(indexPath)
            
            if var size = itemPhoto.thumbnail?.size {
                size.width  += 20
                size.height += 20
                return size
            }
            return CGSize(width: 100, height: 100)
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets {
            return sectionInsets
    }
}



















