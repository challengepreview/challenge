//
//  Menu.swift
//  ChallengePreview
//
//  Created by Erick Ricardo Mattos on 13/07/15.
//  Copyright (c) 2015 Erick Ricardo Mattos. All rights reserved.
//

import Foundation
import CoreData

class Menu: NSManagedObject {

    @NSManaged var descriptionMenu: String
    @NSManaged var imageMenuL: String
    @NSManaged var imageMenuP: String
    @NSManaged var locked: NSNumber
    @NSManaged var numberMasterReleased : NSNumber
    @NSManaged var menuPhases: NSSet
    
    convenience init()
    {
        // get context
        let context:NSManagedObjectContext = DatabaseManager.sharedInstance.managedObjectContext!
        
        // create entity description
        let entityDescription:NSEntityDescription? = NSEntityDescription.entityForName("Menu", inManagedObjectContext: context)
        
        // call super using
        self.init(entity: entityDescription!, insertIntoManagedObjectContext: context)
    }
}
