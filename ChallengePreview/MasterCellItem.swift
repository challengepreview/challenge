//
//  MasterCellItem.swift
//  ChallengePreview
//
//  Created by Erick Ricardo Mattos on 07/07/15.
//  Copyright (c) 2015 Erick Ricardo Mattos. All rights reserved.
//

import UIKit

class MasterCellItem {
    var imageName       : String
    var thumbnail       : UIImage!
    
    init(imageName: String){
        self.imageName      = imageName
        self.thumbnail      = UIImage(named: imageName)!
    }
}