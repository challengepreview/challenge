//
//  PhaseGameImplementation.swift
//  ChallengePreview
//
//  Created by José Ernesto Stelzer Monar on 07/07/15.
//  Copyright (c) 2015 Erick Ricardo Mattos. All rights reserved.
//

import Foundation

protocol PhaseProgressDelegate: class{
    func updateProgress(newProgress: Double)
}

class PhaseGameImplementation: PhaseGame {
    var phaseName : Menu
    var master  = [String]()
    var slaveTip  = Dictionary<String, String>()
    var slaveDescription  = Dictionary<String, String>()
    var dependents = Dictionary<String, (master: String, countable: Bool)>()
    var dependentsCompleted = Dictionary<String, String>()
    var dependentsAttempt = Dictionary<String, Int>()
    
    weak var progressDelegate :PhaseProgressDelegate!
    
    private let maxScoreDeserved: Int
    private var scoreLocal: Int
    private var creditLocal: Int
    private var progress: Double
    
    required init(phaseMenu phaseName:Menu){
        self.phaseName = phaseName
        var results = PhaseDAO.findAllForPhase(phaseName)
        
        results = results.filter({$0.showMaster.boolValue})
        
        for phase : Phase in results {
            let dependent = NSLocalizedString(phase.dependent, comment:"")
            if !contains(master, phase.master) {
                self.master.append(phase.master)
            }
            if !phase.completed.boolValue {
                self.dependents[dependent]            = (master: phase.master, countable: phase.countable.boolValue)
                self.dependentsAttempt[dependent]     = phase.attempts.integerValue
            }else{
                self.dependentsCompleted[dependent]   = phase.master
            }
            
            if phase.showedTip.boolValue {
                self.slaveTip[dependent]                  = NSLocalizedString(phase.tip, comment:"")
            }else{
                self.slaveTip[dependent]                  = ""
            }
            
            self.slaveDescription[dependent]          = NSLocalizedString(phase.masterDescription, comment:"")
        }
        
        self.maxScoreDeserved = Int(ceil(CGFloat(self.master.count)/2))
        self.scoreLocal = 0
        self.creditLocal = 0
        self.progress = Double(self.dependentsCompleted.count)/Double(self.dependents.count+self.dependentsCompleted.count)
    }
    
    private func setupLevel(){
        var results = PhaseDAO.findAllForPhase(self.phaseName)
        
        results = results.filter({$0.showMaster.boolValue})
        
        for phase : Phase in results {
            let dependent = NSLocalizedString(phase.dependent, comment:"")
            if !contains(master, phase.master) {
                self.master.append(phase.master)
            }
            if !phase.completed.boolValue {
                self.dependents[dependent]            = (master: phase.master, countable: phase.countable.boolValue)
                self.dependentsAttempt[dependent]     = phase.attempts.integerValue
            }else{
                self.dependentsCompleted[dependent]   = phase.master
            }
            
            if phase.showedTip.boolValue {
                self.slaveTip[dependent]                  = NSLocalizedString(phase.tip, comment:"")
            }else{
                self.slaveTip[dependent]                  = ""
            }
            
            self.slaveDescription[dependent]          = NSLocalizedString(phase.masterDescription, comment:"")
            
        }
        
        self.scoreLocal = 0
        self.creditLocal = 0
        self.progress = Double(self.dependentsCompleted.count)/Double(self.dependents.count+self.dependentsCompleted.count)
        
        if self.progressDelegate != nil{
            self.progressDelegate.updateProgress(self.progress)
        }
    }
    
    func getMasterImagesName()  -> [String]{
        return master.shuffled()
    }
    
    func getSlaveImagesName()   -> [(name: String, tip: String?)]{
        let slaves = self.dependents.keys.array.shuffled()
        
        return slaves.map {(name: $0, tip: self.slaveTip[$0])}
    }
    
    func isRelated(imageMaster: String, imageSlave: String) -> Bool{
        if self.dependentsAttempt.indexForKey(imageSlave) == nil{
            fatalError("Imagem nao existente")
        }
        
        var extra = 0
        
        self.dependentsAttempt[imageSlave]! += 1
        
        PhaseDAO.increseAttemptsPhase(self.phaseName, master: self.dependents[imageSlave]!.master, dependent: imageSlave)
        
        let itsRight: Bool = ( self.dependents[imageSlave]!.master == imageMaster )
        
        if self.dependents[imageSlave]!.countable {
        
            if itsRight{
                self.scoreLocal += self.maxScoreDeserved
                
                switch self.dependentsAttempt[imageSlave]! {
                case 1:
                    extra = 20
                case 2:
                    extra = 10
                case 3:
                    extra = 05
                default:
                    extra = 00
                }
                
                PlayerDAO.updateValues(PlayerDAO.getScore() + self.maxScoreDeserved, credit: PlayerDAO.getCredit() + extra)
            }
            else {
                self.scoreLocal -= 1
                
                PlayerDAO.updateValues(PlayerDAO.getScore() - 1)
            }
        }
        
        if itsRight {
            self.dependentsCompleted[imageSlave] = imageMaster
            self.dependents.removeValueForKey(imageSlave)
            PhaseDAO.completeImagePhase(self.phaseName, master: imageMaster, dependent: imageSlave)
            self.progress = Double(self.dependentsCompleted.count)/Double(self.dependents.count+self.dependentsCompleted.count)
            self.progressDelegate.updateProgress(self.progress)
        }
        
        return itsRight
    }
    
    func getLocalScore()    -> Int{
        return self.scoreLocal
    }
    
    func getGlobalScore()   -> Int{
        return ( PlayerDAO.getScore() )
    }
    
    func getProgress() -> Double{
        return self.progress
    }
    
    func completedMaster(master: String) -> Bool{
        return (self.dependents.values.filter({$0.master == master}).array.count == 0)
    }
    
    func removeMaster(master: String) -> Bool{
        return PhaseDAO.removeMasterFromList(self.phaseName, master: master, reallyRemove: completedMaster(master))
    }
    
    func shouldShowCompletedSlaveMaster(master: String) -> Bool {
        return PhaseDAO.shouldShowCompletedImageListForMaster(self.phaseName, master: master)
    }
    
    func showCompletedSlaveMaster(master: String) -> Bool{
        return PhaseDAO.showCompletedImageListForMaster(self.phaseName, master: master)
    }
    
    func getCompletedSlaveForMaster(master: String) -> [(name: String, description: String?)]{
        if !shouldShowCompletedSlaveMaster(master) {
            return []
        }
        
        var slaves: [String] = [String]()
        for key in self.dependentsCompleted.keys {
            if self.dependentsCompleted[key] == master{
                slaves.append(key)
            }
        }
        
        return slaves.map {(name: $0, tip: self.slaveDescription[$0])}
    }
    
    func getCustForHelpX() -> Int{
        return PlayerDAO.calculateTakeX(self.phaseName.numberMasterReleased.integerValue + 1, A: PlayerDAO.AOthers)
    }
    
    
    func getCustForHelpTip() -> Int{
        return PlayerDAO.calculateTakeTip()
    }
    
    func requestMoreCredit(quantityFinal: Int) -> Bool{
        return PlayerDAO.requestMoreCredit(quantityFinal)
    }
    
    func shouldShowTip(slave: String) -> Bool{
        return self.slaveTip[slave] != ""
    }
    
    func tryShowTip(slave: String) -> String{
        if shouldShowTip(slave){
            return self.slaveTip[slave]!
        }
        self.slaveTip[slave] = PhaseDAO.canReleaseTip(self.phaseName, master: self.dependents[slave]!.master, dependent: slave)
        return self.slaveTip[slave]!
    }
    
    func setProgressDelegate(delegate : PhaseProgressDelegate) {
        self.progressDelegate = delegate
        self.progressDelegate?.updateProgress(self.progress)

    }
    
    func reset() {
        PhaseDAO.resetPhase(self.phaseName)
        self.setupLevel()
    }
}