//
//  MasterCollectionView.swift
//  ChallengePreview
//
//  Created by Erick Ricardo Mattos on 06/07/15.
//  Copyright (c) 2015 Erick Ricardo Mattos. All rights reserved.
//

import UIKit

class MasterCollectionView: NSObject {
    
    private let velocityAutoScroll:     CGFloat                 =  50
    var directionScroll:                Direction               = .None
    
    private let reuseIdentifier = "MasterCell"
    
    private var imagesMaster:[MasterCellItem] = [MasterCellItem]()
    private var maxHeight: CGFloat = -10.0
    
    private var whoCreateMe: CollectionCellDelegate!
    
    func photoForIndexPath(indexPath: NSIndexPath) -> MasterCellItem {
        return imagesMaster[indexPath.row]
    }
    
    private override init(){
        
    }
    
    init(imagesName: [String], who: CollectionCellDelegate){
        super.init()
        self.whoCreateMe = who
        for image in imagesName {
            self.imagesMaster.insert(MasterCellItem(imageName: image), atIndex: 0)
        }
    }
    
    func resetLevel(imagesName: [String]){
        self.imagesMaster.removeAll(keepCapacity: false)
        for image in imagesName {
            self.imagesMaster.insert(MasterCellItem(imageName: image), atIndex: 0)
        }
    }
    
    func scrollAutomaticALitBit(){
        if self.directionScroll != .None{
            let collectionView: UICollectionView = (self.whoCreateMe as! GameViewController).masterCollectionView
            
            let newXOffset = collectionView.contentOffset.x + (CGFloat(self.directionScroll.rawValue/2) * self.velocityAutoScroll)
            
            if (newXOffset < collectionView.contentSize.width - collectionView.frame.size.width)&&( newXOffset > 0 ){
                collectionView.setContentOffset(CGPoint(x: newXOffset, y: collectionView.contentOffset.y ), animated: true)
            }
        }
    }
    
    func removeImageMaster(master: String){
        self.imagesMaster = self.imagesMaster.filter({$0.imageName != master})
    }
}

extension MasterCollectionView : UICollectionViewDelegateFlowLayout {
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let vc = (self.whoCreateMe as! GameViewController)
        let attributes = collectionView.layoutAttributesForItemAtIndexPath(indexPath)
        if attributes != nil{
            let cellFrameInSuperview = collectionView.convertRect(attributes!.frame, toView: collectionView.superview)
            
            var s: CGFloat = 0
            
            if cellFrameInSuperview.origin.x > collectionView.superview!.frame.size.width - cellFrameInSuperview.size.width {
                s = cellFrameInSuperview.origin.x + cellFrameInSuperview.size.width - collectionView.superview!.frame.size.width
            }
            
            vc.viewAncora.frame.origin.x = cellFrameInSuperview.origin.x + cellFrameInSuperview.size.width - vc.viewAncora.frame.size.width - s
            
            vc.viewAncora.frame.origin.y = cellFrameInSuperview.origin.y + cellFrameInSuperview.size.height - vc.viewAncora.frame.size.height

            vc.setMasterName(photoForIndexPath(indexPath).imageName)
            vc.performSegueWithIdentifier(vc.listCompleted, sender: vc)
        }
        
        /*
        let actionSheet: UIActionSheet = UIActionSheet(title: NSLocalizedString("Solicitar ajuda.", comment: "Mensagem para solicitar ajuda sem ser uma pergunta"), delegate: self.whoCreateMe as! GameViewController, cancelButtonTitle: nil, destructiveButtonTitle: NSLocalizedString("Não preciso de ajuda.", comment: "Mensagem para negar ajuda"), otherButtonTitles: NSLocalizedString("Exibir respostas já encontradas.", comment: "Mensagem para exibir as imagens escravas jah encontradas para uma imagem mestre"), NSLocalizedString("Remover uma imagem.", comment: "Mensagem para remover a imagem mestre"))
        actionSheet.tag = indexPath.row/* saber quem foi selecionada */
        actionSheet.showInView(collectionView.superview)*/
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            let photo = photoForIndexPath(indexPath)
            if var imageMaster = photo.thumbnail {
                var size = imageMaster.size
                
                size.width *= (collectionView.frame.size.height)/size.height
                size.height = (collectionView.frame.size.height)
                
                if size.height > self.maxHeight {
                    self.maxHeight = size.height
                }
                
                return size
            }
            return CGSize(width: 100, height: 100)
    }
    
    /*func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets {
            let spaceTopB = collectionView.frame.size.height - self.maxHeight
            return UIEdgeInsets(top: spaceTopB/2, left: 10.0, bottom: 0, right: 10.0)
    }*/
}

extension MasterCollectionView : UICollectionViewDataSource {
    
    /* numero de itens no menu */
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    /* numero de imagens por menu */
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesMaster.count
    }
    
    /* configurar as celulas */
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! MasterCell
        // Configure the cell
        let menuPhoto = photoForIndexPath(indexPath)
        
        cell.myself = menuPhoto
        
        cell.imageView.image = menuPhoto.thumbnail
        cell.backgroundColor = UIColor.whiteColor()
        
        //cell.layer.cornerRadius = cell.frame.size.width/2
        
        return cell
    }
}

extension MasterCollectionView : UIScrollViewDelegate {
    func scrollViewDidScroll(scrollView: UIScrollView){
        self.scrollAutomaticALitBit()
    }
}