//
//  PlayerServices.swift
//  ChallengePreview
//
//  Created by Erick Ricardo Mattos on 14/07/15.
//  Copyright (c) 2015 Erick Ricardo Mattos. All rights reserved.
//

import Foundation

class PlayerServices
{
    private static let namePlayer:     String      = UIDevice.currentDevice().identifierForVendor.UUIDString
    private static let scoreInitial:   NSNumber    = NSNumber(int: 0)
    private static let creditInitial:  NSNumber    = NSNumber(int: 100)
    
    static func createPlayer()
    {
        var player: Player      = Player()
        player.name             = namePlayer
        player.score            = scoreInitial
        player.credit           = creditInitial
        player.numberPhasePay   = NSNumber(integer: 0)
        // insert it
        PlayerDAO.insert(player)
        
        return
    }
}