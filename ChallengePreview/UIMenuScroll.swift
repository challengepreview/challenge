//
//  ScrollViewController.swift
//  UIMenuScroll
//
//  Created by Antonio Carlos Guimarães Junior on 7/17/15.
//  Copyright (c) 2015 Erick Ricardo Mattos. All rights reserved.
//

import UIKit
let angularConst: CGFloat = 1.0
let evalDist: CGFloat = 10

class UIMenuScroll: UIView {
    var collection: UICollectionView!
    var atCell: MenuKindCell!
    var atCell2: SlaveCell!
    var iniPoint: CGPoint!
    var isTap = false
    var path: NSIndexPath!
    var orientation: Int = 0
    var dragging = false
    var scrolling = false
    var prevTimeStamp: NSTimeInterval!
    var parentClass: GameViewController!
    var jaFoi: Bool = false
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    func acoxambrar(){
        if(jaFoi){
            return
        }
        jaFoi = true
        self.iniPoint = CGPointMake(collection.frame.origin.x + 0.05 * collection.frame.width, collection.frame.origin.y + 0.5 * collection.frame.height)
        path = self.collection.indexPathForItemAtPoint(CGPointMake(0.05 * collection.frame.width, 0.5 * collection.frame.height))
        if path == nil {
            println("erro")
            return
        }
        var atPoint = self.iniPoint
        let index = path
        // began
        var cell = self.collection.cellForItemAtIndexPath(index!) as? MenuKindCell?
        if(cell == nil){
            var cell = self.collection.cellForItemAtIndexPath(index!) as? SlaveCell?
            atCell2 = cell!
            parentClass.touchBegan(self.iniPoint, withCell: atCell2)
        }else{
            atCell = cell!
            atCell.touchBegan(atPoint)
        }
        // moved
        if(atCell != nil){
            atCell.touchMoved(self.iniPoint)
        }
        if(atCell2 != nil){
            parentClass.touchMoved(self.iniPoint, withCell: atCell2)
        }
        // ended
        if(atCell != nil){
            atCell.touchEnded(self.iniPoint)
            atCell = nil
        }
        if(atCell2 != nil){
            parentClass.touchEnded(self.iniPoint, withCell: atCell2)
            atCell2 = nil
        }
        atCell = nil
        atCell2 = nil
        dragging = false
        scrolling = false
        path = nil
        self.iniPoint = nil
    
    }


    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        let touch: UITouch = touches.first as! UITouch
        let newPosition = touch.locationInView(self.collection.superview!)
        isTap = true
        
            self.iniPoint = newPosition
            path = self.collection.indexPathForItemAtPoint(touch.locationInView(self.collection))
        /*else if(event.allTouches()!.count == 2){
            var atPoint = CGPointZero
            for t in event.allTouches()! {
                let touch = t as! UITouch
                let location = touch.locationInView(self.collection)
                atPoint.x += location.x
                atPoint.y += location.y
            }
            atPoint.x /= CGFloat(event.allTouches()!.count)
            atPoint.y /= CGFloat(event.allTouches()!.count)
            let index = self.collection.indexPathForItemAtPoint(atPoint)
            if index == nil{
                return
            }
            var cell = self.collection.cellForItemAtIndexPath(index!) as? MenuKindCell?
            if(cell == nil){
                var cell = self.collection.cellForItemAtIndexPath(index!) as? SlaveCell?
                atCell2 = cell!
                atCell2.touchBegan(atPoint)
                return
            }
            atCell = cell!
            atCell.touchBegan(atPoint)
        }*/
    }
    
    
    override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {
        let touch: UITouch = touches.first as! UITouch
        let newPosition = touch.locationInView(self.collection.superview!)
        isTap = false
        prevTimeStamp = event.timestamp
        
        if(dragging){
            if(atCell != nil){
                atCell.touchMoved(newPosition)
                return
            }
            if(atCell2 != nil){
                parentClass.touchMoved(newPosition, withCell: atCell2)
                return
            }
        }
        if(!scrolling && sqrt(pow(self.iniPoint.y - newPosition.y,2) + pow((newPosition.x - self.iniPoint.x),2)) < evalDist){
            return
        }
        
        if(self.iniPoint != nil){
            var newPos = newPosition
            if(!scrolling && angularConst*(self.iniPoint.y - newPos.y) > fabs(newPos.x - self.iniPoint.x)){
                dragging = true
                var atPoint = self.iniPoint
                let index = path
                if index == nil{
                    return
                }
                var cell = self.collection.cellForItemAtIndexPath(index!) as? MenuKindCell?
                if(cell == nil){
                    var cell = self.collection.cellForItemAtIndexPath(index!) as? SlaveCell?
                    atCell2 = cell!
                    parentClass.touchBegan(newPosition, withCell: atCell2)
                    return
                }
                atCell = cell!
                atCell.touchBegan(atPoint)
                return
            }
            scrolling = true
            if(orientation == 0){
                if ((self.collection.contentOffset.y + self.iniPoint.y - newPos.y) < self.collection.contentSize.height - self.collection.frame.size.height)&&( (self.collection.contentOffset.y + self.iniPoint.y - newPos.y)  > 0 ){
                    self.collection.setContentOffset(CGPointMake(self.collection.contentOffset.x, self.collection.contentOffset.y + self.iniPoint.y - newPos.y), animated: false)
                }
                self.iniPoint.y = newPos.y
            }else{
                if ((self.collection.contentOffset.x + self.iniPoint.x - newPos.x) < self.collection.contentSize.width - self.collection.frame.size.width) && ( (self.collection.contentOffset.x + self.iniPoint.x - newPos.x)  > 0 ){
                    self.collection.setContentOffset(CGPointMake(self.collection.contentOffset.x + self.iniPoint.x - newPos.x, self.collection.contentOffset.y), animated: false)
                }
                self.iniPoint.x = newPos.x
            }
        }else{
            //println("Reinicia")
            self.iniPoint = newPosition
            if(atCell != nil){
                atCell.touchEnded(newPosition)
                atCell = nil
            }
            if(atCell2 != nil){
                parentClass.touchEnded(newPosition, withCell: atCell2)
                atCell2 = nil
            }
        }/*else if(event.allTouches()!.count > 1 && (atCell != nil || atCell2 != nil)){
            var newPos = CGPointZero
            for t in event.allTouches()! {
                let touch = t as! UITouch
                let location = touch.locationInView(self.collection.superview!)
                newPos.x += location.x
                newPos.y += location.y
            }
            newPos.x /= CGFloat(event.allTouches()!.count)
            newPos.y /= CGFloat(event.allTouches()!.count)
            if(atCell != nil){
                atCell.touchMoved(newPos)
            }else{
                atCell2.touchMoved(newPos)
            }
        }*/
    }
    
    
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        let touch: UITouch = touches.first as! UITouch
        let newPosition = touch.locationInView(self.collection.superview!)
        /* var a:Double = 30
        if(scrolling){
            if(orientation == 0){
                var vel = Double(newPosition.y - self.iniPoint.y) / Double(event.timestamp - prevTimeStamp)
                var pos = Double(self.collection.contentOffset.y) + vel - a/2.0
                if(pos > Double(self.collection.contentSize.height - self.collection.frame.size.height)){
                    pos = Double(self.collection.contentSize.height - self.collection.frame.size.height)
                }
                if(pos < 0){
                    pos = 0.0
                }
                self.collection.setContentOffset(CGPointMake(self.collection.contentOffset.x, CGFloat(pos)), animated: true)
            }else{
                var vel = Double(newPosition.x - self.iniPoint.x) / Double(event.timestamp - prevTimeStamp)
                if(vel > 0){
                    a = -1.0*a // ???
                }
                var t = vel/a
                var pos = Double(self.collection.contentOffset.x) + vel*t - a*t*t/2.0
                if(pos > Double(self.collection.contentSize.width - self.collection.frame.size.width)){
                   pos = Double(self.collection.contentSize.width - self.collection.frame.size.width)
                }
                if(pos < 0){
                    pos = 0.0
                }
                self.collection.setContentOffset(CGPointMake(CGFloat(pos), self.collection.contentOffset.y), animated: true)
            }
        } */
        if(atCell != nil){
            atCell.touchEnded(newPosition)
            atCell = nil
        }
        if(atCell2 != nil){
            parentClass.touchEnded(newPosition, withCell: atCell2)
            //atCell2.touchEnded(newPosition)
            atCell2 = nil
        }
        if(isTap){
            //self.collection.select(atCell)
            if(path == nil){
                return
            }
            self.collection.delegate?.collectionView!(self.collection, didSelectItemAtIndexPath:path)
            print("tap")
        }
        atCell = nil
        atCell2 = nil
        dragging = false
        scrolling = false
        path = nil
        
        self.iniPoint = nil
    }

}
