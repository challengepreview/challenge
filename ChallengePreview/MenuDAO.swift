//
//  MenuDAO.swift
//  ChallengePreview
//
//  Created by Erick Ricardo Mattos on 06/07/15.
//  Copyright (c) 2015 Erick Ricardo Mattos. All rights reserved.
//

import Foundation
import CoreData

class MenuDAO
{
    static func getAllMenu() -> [Menu]
    {
        // creating fetch request
        let request = NSFetchRequest(entityName: "Menu")
        
        // assign sort descriptor
        request.sortDescriptors = [NSSortDescriptor(key: "locked", ascending:false), NSSortDescriptor(key: "descriptionMenu", ascending:false)]
        
        // perform search
        var error:NSErrorPointer = nil
        let results:[Menu] = DatabaseManager.sharedInstance.managedObjectContext?.executeFetchRequest(request, error: error) as! [Menu]
        
        return results
    }
    
    static func findByDescription(name: String) -> Menu?
    {
        // creating fetch request
        let request = NSFetchRequest(entityName: "Menu")
        
        // assign predicate
        request.predicate = NSPredicate(format: "descriptionMenu == %@", name)
        
        // perform search
        var error:NSErrorPointer = nil
        let results:[Menu] = DatabaseManager.sharedInstance.managedObjectContext?.executeFetchRequest(request, error: error) as! [Menu]
        if(results.count > 0){
            return results[0]
        }else{
            return nil;
        }
    }
    
    static func unlockAnyPhase(){
        // creating fetch request
        let request = NSFetchRequest(entityName: "Menu")
        
        // assign predicate
        request.predicate = NSPredicate(format: "locked == %@", NSNumber(bool: true))
        // perform search
        var error:NSErrorPointer = nil
        var results:[Menu] = DatabaseManager.sharedInstance.managedObjectContext?.executeFetchRequest(request, error: error) as! [Menu]
        
        let count = UInt32(results.count)
        
        if count > 0{
            let i = arc4random_uniform(count)
            let new: Menu = results[Int(i)]
            new.setValue(NSNumber(bool: false), forKey: "locked")
            
            DatabaseManager.sharedInstance.managedObjectContext?.save(error)
            if (error != nil)
            {
                // log error
                print(error)
            }
            
            return
        }else{
            return
        }
    }
    
    static func unlockPhase(phaseName: String) -> Bool{
        if let new: Menu = findByDescription(phaseName) {
            if new.locked.boolValue {
                new.setValue(NSNumber(bool: false), forKey: "locked")
                var error:NSErrorPointer = nil
                DatabaseManager.sharedInstance.managedObjectContext?.save(error)
                if (error != nil)
                {
                    // log error
                    print(error)
                }
                
                return true
            }
            else{
                return false
            }
        }
        return false
    }
    
    static func unlockPhasePay(phaseName: String) -> Bool{
        return true
    }
    
    static func insert(objectToBeInserted:Menu)
    {
        // insert element into context
        DatabaseManager.sharedInstance.managedObjectContext?.insertObject(objectToBeInserted)
        
        // save context
        var error:NSErrorPointer = nil
        DatabaseManager.sharedInstance.managedObjectContext?.save(error)
        if (error != nil)
        {
            // log error
            print(error)
        }
    }
    
    static func delete(objectToBeDeleted:Menu)
    {
        // remove object from context
        var error:NSErrorPointer = nil
        DatabaseManager.sharedInstance.managedObjectContext?.deleteObject(objectToBeDeleted)
        DatabaseManager.sharedInstance.managedObjectContext?.save(error)
        
        // log error
        if (error != nil)
        {
            // log error
            print(error)
        }
    }
    
    
    
}