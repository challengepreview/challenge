//
//  ScoreView.swift
//  ChallengePreview
//
//  Created by Erick Ricardo Mattos on 16/07/15.
//  Copyright (c) 2015 Erick Ricardo Mattos. All rights reserved.
//

import UIKit

protocol ScoreViewDelegate{
    func buttomTaped()
}

class ScoreView: UIView {
    private var delegateTap: ScoreViewDelegate!
    
    private let score   = NSLocalizedString("SCORE: %d",  comment: "Expressao para mostrar a pontuacao do jogo")
    private let credit  = NSLocalizedString("CREDIT: %d", comment: "Expressao para mostrar a quantidade de credito do jogo")
    
    private let progress = "%3d%%"
    
    private var btnBack:        UIButton!
    private var lblScore:       UILabel!
    private var lblCredit:      UILabel!
    private var lblProgress:    UILabel!
    private var imVScore:       UIImageView!
    private var imVCredit:      UIImageView!
    private var viewBlur:       UIVisualEffectView!
    private var viewVibrancy:   UIVisualEffectView!
    private var cpVProgress:    CircleProgressView!
    
    @IBInspectable var showBackBtn:     Bool    = true
    
    @IBInspectable var scoreColor:      UIColor = UIColor.whiteColor()
    @IBInspectable var creditColor:     UIColor = UIColor.whiteColor()
    @IBInspectable var baseColor:       UIColor = UIColor.blackColor()
    @IBInspectable var titleFont:       String  = "HelveticaNeue-Bold"
    @IBInspectable var titleSize:       CGFloat = 15
    @IBInspectable var blurStyle:       UIBlurEffectStyle = UIBlurEffectStyle.Dark
    @IBInspectable var imgScore0:       String  = "whiteCheck"
    @IBInspectable var imgScoreP:       String  = "greenCheck"
    @IBInspectable var imgScoreN:       String  = "redCheck"
    @IBInspectable var imgCredit:       String  = "coins"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
        
        /* exibir score */
        PlayerDAO.addObserverPlayer(self)
    }
    
    override func prepareForInterfaceBuilder() {
        setupView()
    }
    
    func setupView(){
        /* configura View */
        backgroundColor = UIColor.clearColor()
        
        /* Blur background effect */
        let blur = UIBlurEffect(style: blurStyle)
        viewBlur = UIVisualEffectView(effect: blur)
        addSubview(viewBlur)
        
        viewVibrancy = UIVisualEffectView(effect: UIVibrancyEffect(forBlurEffect: blur))
        viewBlur.contentView.addSubview(viewVibrancy)
        
        /* configura imageView Score */
        imVScore = UIImageView(frame: CGRect(x: 0, y: 0, width: 100, height: 30))
        imVScore.backgroundColor = UIColor.clearColor()
        imVScore.contentMode = .Center
        imVScore.image = UIImage(named: imgScore0)
        viewVibrancy.contentView.addSubview(imVScore)
        
        /* configura label Score */
        lblScore = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 30))
        lblScore.backgroundColor = UIColor.clearColor()
        lblScore.font = UIFont(name: titleFont, size: titleSize)
        lblScore.textColor = scoreColor
        lblScore.text = NSString(format: score, 0) as String
        viewVibrancy.contentView.addSubview(lblScore)
        
        /* configura progress circle view */
        cpVProgress = CircleProgressView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        cpVProgress.progress = 0.5
        cpVProgress.centerFillColor = UIColor.darkGrayColor()
        cpVProgress.backgroundColor = UIColor.clearColor()
        cpVProgress.hidden = true
        addSubview(cpVProgress)
        
        /* configura label Score */
        lblProgress = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 30))
        lblProgress.backgroundColor = UIColor.clearColor()
        lblProgress.font = UIFont(name: titleFont, size: titleSize)
        lblProgress.textColor = scoreColor
        lblProgress.text = NSString(format: progress, 50) as String
        lblProgress.hidden = true
        viewVibrancy.contentView.addSubview(lblProgress)
        
        /* btnBack */
        if showBackBtn {
            btnBack = UIButton(frame: CGRect(x: 0, y: 0, width: 100, height: 30))
            btnBack.titleLabel!.font = UIFont(name: titleFont, size: titleSize)
            btnBack.titleLabel!.textColor = scoreColor
            btnBack.setTitle("Voltar", forState: .Normal)
            btnBack.hidden = false
            viewVibrancy.contentView.addSubview(btnBack)
            
            
            var tapGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("handleTapFrom:"))
            btnBack.addGestureRecognizer(tapGestureRecognizer)
            tapGestureRecognizer.delegate = self
        }
        
        /* configura imageView Credit */
        imVCredit = UIImageView(frame: CGRect(x: 0, y: 0, width: 100, height: 30))
        imVCredit.backgroundColor = UIColor.clearColor()
        imVCredit.contentMode = .Center
        imVCredit.image = UIImage(named: imgCredit)
        viewVibrancy.contentView.addSubview(imVCredit)
        
        /* configura label Credit */
        lblCredit = UILabel(frame: CGRect(x: 100, y: 0, width: 100, height: 30))
        lblCredit.backgroundColor = UIColor.clearColor()
        lblCredit.font = UIFont(name: titleFont, size: titleSize)
        lblCredit.textColor = creditColor
        lblCredit.text = NSString(format: credit, 0) as String
        viewVibrancy.contentView.addSubview(lblCredit)
        
        /* configuracao Constrains */
        viewBlur.setTranslatesAutoresizingMaskIntoConstraints(false)
        
        let blurTop        = NSLayoutConstraint(item: viewBlur, attribute: .Top      , relatedBy: .Equal, toItem: self, attribute: .Top,       multiplier:   1, constant: 0)
        let blurBottom     = NSLayoutConstraint(item: viewBlur, attribute: .Bottom   , relatedBy: .Equal, toItem: self, attribute: .Bottom,    multiplier:   1, constant: 0)
        let blurLeading    = NSLayoutConstraint(item: viewBlur, attribute: .Leading  , relatedBy: .Equal, toItem: self, attribute: .Leading,   multiplier:   1, constant: 0)
        let blurTrailing   = NSLayoutConstraint(item: viewBlur, attribute: .Trailing , relatedBy: .Equal, toItem: self, attribute: .Trailing,  multiplier:   1, constant: 0)
        
        addConstraint(blurTop)
        addConstraint(blurBottom)
        addConstraint(blurLeading)
        addConstraint(blurTrailing)
        
        viewVibrancy.setTranslatesAutoresizingMaskIntoConstraints(false)
        
        let vibrancyTop        = NSLayoutConstraint(item: viewVibrancy, attribute: .Top      , relatedBy: .Equal, toItem: viewBlur, attribute: .Top,       multiplier:   1, constant: 0)
        let vibrancyBottom     = NSLayoutConstraint(item: viewVibrancy, attribute: .Bottom   , relatedBy: .Equal, toItem: viewBlur, attribute: .Bottom,    multiplier:   1, constant: 0)
        let vibrancyLeading    = NSLayoutConstraint(item: viewVibrancy, attribute: .Leading  , relatedBy: .Equal, toItem: viewBlur, attribute: .Leading,   multiplier:   1, constant: 0)
        let vibrancyTrailing   = NSLayoutConstraint(item: viewVibrancy, attribute: .Trailing , relatedBy: .Equal, toItem: viewBlur, attribute: .Trailing,  multiplier:   1, constant: 0)
        
        addConstraint(vibrancyTop)
        addConstraint(vibrancyBottom)
        addConstraint(vibrancyLeading)
        addConstraint(vibrancyTrailing)
        
        lblProgress.setTranslatesAutoresizingMaskIntoConstraints(false)
        
        let progressWidth       = NSLayoutConstraint(item: lblProgress, attribute: .Width    , relatedBy: .Equal, toItem: lblProgress,  attribute: .Width,     multiplier:   1, constant: 100)
        let progressTop         = NSLayoutConstraint(item: lblProgress, attribute: .Top      , relatedBy: .Equal, toItem: viewVibrancy, attribute: .Top,       multiplier:   1, constant: 0)
        let progressBottom      = NSLayoutConstraint(item: lblProgress, attribute: .Bottom   , relatedBy: .Equal, toItem: viewVibrancy, attribute: .Bottom,    multiplier:   1, constant: 0)
        let progressTrailing    = NSLayoutConstraint(item: lblProgress, attribute: .Trailing , relatedBy: .Equal, toItem: viewVibrancy, attribute: .Trailing,  multiplier:   1, constant: -5)
        
        addConstraint(progressWidth)
        addConstraint(progressTop)
        addConstraint(progressBottom)
        addConstraint(progressTrailing)
        
        cpVProgress.setTranslatesAutoresizingMaskIntoConstraints(false)
        
        let progressIWidth      = NSLayoutConstraint(item: cpVProgress, attribute: .Width    , relatedBy: .Equal, toItem: viewVibrancy, attribute: .Height,    multiplier:   1, constant: 0)
        let progressITop        = NSLayoutConstraint(item: cpVProgress, attribute: .Top      , relatedBy: .Equal, toItem: viewVibrancy, attribute: .Top,       multiplier:   1, constant: 0)
        let progressIBottom     = NSLayoutConstraint(item: cpVProgress, attribute: .Bottom   , relatedBy: .Equal, toItem: viewVibrancy, attribute: .Bottom,    multiplier:   1, constant: 0)
        let progressITrailing   = NSLayoutConstraint(item: cpVProgress, attribute: .Trailing , relatedBy: .Equal, toItem:  lblProgress, attribute: .Leading,   multiplier:   1, constant: -5)
        
        addConstraint(progressITop)
        addConstraint(progressIBottom)
        addConstraint(progressIWidth)
        addConstraint(progressITrailing)
        
        var viewLefter: UIView = viewVibrancy
        var atributeLefter: NSLayoutAttribute = .Leading
        
        if showBackBtn {
            viewLefter = btnBack
            atributeLefter = .Trailing
            
            btnBack.setTranslatesAutoresizingMaskIntoConstraints(false)
            
            let backWidth      = NSLayoutConstraint(item: btnBack, attribute: .Width    , relatedBy: .Equal, toItem: viewVibrancy, attribute: .Width,     multiplier: 0.125, constant: 0)
            let backTop        = NSLayoutConstraint(item: btnBack, attribute: .Top      , relatedBy: .Equal, toItem: viewVibrancy, attribute: .Top,       multiplier:     1, constant: 0)
            let backBottom     = NSLayoutConstraint(item: btnBack, attribute: .Bottom   , relatedBy: .Equal, toItem: viewVibrancy, attribute: .Bottom,    multiplier:     1, constant: 0)
            let backLeading    = NSLayoutConstraint(item: btnBack, attribute: .Leading  , relatedBy: .Equal, toItem: viewVibrancy, attribute: .Leading,   multiplier:     1, constant: 0)
            
            backWidth.priority = 1000
            
            addConstraint(backWidth)
            addConstraint(backTop)
            addConstraint(backBottom)
            addConstraint(backLeading)
        }
        
        imVScore.setTranslatesAutoresizingMaskIntoConstraints(false)
        
        let scoreIWidth      = NSLayoutConstraint(item: imVScore, attribute: .Width    , relatedBy: .Equal, toItem: imVScore,       attribute: .Width,           multiplier:      1, constant: 30)
        let scoreITop        = NSLayoutConstraint(item: imVScore, attribute: .Top      , relatedBy: .Equal, toItem: viewVibrancy,   attribute: .Top,             multiplier:      1, constant: 0)
        let scoreIBottom     = NSLayoutConstraint(item: imVScore, attribute: .Bottom   , relatedBy: .Equal, toItem: viewVibrancy,   attribute: .Bottom,          multiplier:      1, constant: 0 )
        let scoreILeading    = NSLayoutConstraint(item: imVScore, attribute: .Leading  , relatedBy: .Equal, toItem: viewLefter,     attribute: atributeLefter,   multiplier:      1, constant: 0)
        
        addConstraint(scoreIWidth)
        addConstraint(scoreITop)
        addConstraint(scoreIBottom)
        addConstraint(scoreILeading)
        
        lblScore.setTranslatesAutoresizingMaskIntoConstraints(false)
        
        let scoreWidth      = NSLayoutConstraint(item: lblScore, attribute: .Width    , relatedBy: .Equal, toItem: viewVibrancy, attribute: .Width,     multiplier: 0.25, constant: 0)
        let scoreTop        = NSLayoutConstraint(item: lblScore, attribute: .Top      , relatedBy: .Equal, toItem: viewVibrancy, attribute: .Top,       multiplier:   1, constant: 0)
        let scoreBottom     = NSLayoutConstraint(item: lblScore, attribute: .Bottom   , relatedBy: .Equal, toItem: viewVibrancy, attribute: .Bottom,    multiplier:   1, constant: 0)
        let scoreLeading    = NSLayoutConstraint(item: lblScore, attribute: .Leading  , relatedBy: .Equal, toItem: imVScore    , attribute: .Trailing,   multiplier:   1, constant: 10)
        
        addConstraint(scoreWidth)
        addConstraint(scoreTop)
        addConstraint(scoreBottom)
        addConstraint(scoreLeading)
        
        imVCredit.setTranslatesAutoresizingMaskIntoConstraints(false)
        
        let creditIWidth      = NSLayoutConstraint(item: imVCredit, attribute: .Width    , relatedBy: .Equal, toItem: imVCredit,        attribute: .Width,      multiplier:      1, constant: 30)
        let creditITop        = NSLayoutConstraint(item: imVCredit, attribute: .Top      , relatedBy: .Equal, toItem: viewVibrancy,     attribute: .Top,        multiplier:   1, constant: 0)
        let creditIBottom     = NSLayoutConstraint(item: imVCredit, attribute: .Bottom   , relatedBy: .Equal, toItem: viewVibrancy,     attribute: .Bottom,     multiplier:   1, constant: 0)
        let creditILeading    = NSLayoutConstraint(item: imVCredit, attribute: .Leading  , relatedBy: .Equal, toItem: lblScore,         attribute: .Trailing,   multiplier:   1, constant: 0)
        
        addConstraint(creditIWidth)
        addConstraint(creditITop)
        addConstraint(creditIBottom)
        addConstraint(creditILeading)
        
        lblCredit.setTranslatesAutoresizingMaskIntoConstraints(false)
        
        let creditWidth      = NSLayoutConstraint(item: lblCredit, attribute: .Width    , relatedBy: .Equal, toItem: viewVibrancy,  attribute: .Width,      multiplier: 0.25, constant: 0)
        let creditTop        = NSLayoutConstraint(item: lblCredit, attribute: .Top      , relatedBy: .Equal, toItem: viewVibrancy,  attribute: .Top,        multiplier:   1, constant: 0)
        let creditBottom     = NSLayoutConstraint(item: lblCredit, attribute: .Bottom   , relatedBy: .Equal, toItem: viewVibrancy,  attribute: .Bottom,     multiplier:   1, constant: 0)
        let creditLeading    = NSLayoutConstraint(item: lblCredit, attribute: .Leading  , relatedBy: .Equal, toItem: imVCredit,  attribute: .Trailing,   multiplier:   1, constant: 0)
        
        addConstraint(creditWidth)
        addConstraint(creditTop)
        addConstraint(creditBottom)
        addConstraint(creditLeading)
    }
    
    func setDelegate(delegate: ScoreViewDelegate){
        self.delegateTap = delegate
        
        cpVProgress.hidden = false
        lblProgress.hidden = false
    }
}

extension ScoreView: UIGestureRecognizerDelegate{
    func handleTapFrom(recognizer: UITapGestureRecognizer){
        if delegateTap != nil{
            delegateTap.buttomTaped()
        }
    }
}

extension ScoreView: PlayerProtocolObserver{
    func setScore(newScore: Int){
        if newScore > 0 {
            imVScore.image = UIImage(named: imgScoreP)
        }else if newScore == 0{
            imVScore.image = UIImage(named: imgScore0)
        }else{
            imVScore.image = UIImage(named: imgScoreN)
        }
        
        UIView.animateKeyframesWithDuration(1.5, delay: 0, options: nil, animations: { () -> Void in
            UIView.addKeyframeWithRelativeStartTime(0, relativeDuration: 0.6, animations: { () -> Void in
                self.lblScore.text = NSString(format: self.score, newScore) as String
                self.lblScore.transform = CGAffineTransformMakeScale(1.3, 1.3)
            })
            UIView.addKeyframeWithRelativeStartTime(0.6, relativeDuration: 0.4, animations: { () -> Void in
                self.lblScore.transform = CGAffineTransformMakeScale(1.0, 1.0)
            })
            }, completion: { (result) -> Void in
                
        })
    }
    
    func setCredit(newCredit: Int){
        UIView.animateKeyframesWithDuration(1.5, delay: 0, options: nil, animations: { () -> Void in
            UIView.addKeyframeWithRelativeStartTime(0, relativeDuration: 0.6, animations: { () -> Void in
                self.lblCredit.text = NSString(format: self.credit, newCredit) as String
                self.lblCredit.transform = CGAffineTransformMakeScale(1.3, 1.3)
            })
            UIView.addKeyframeWithRelativeStartTime(0.6, relativeDuration: 0.4, animations: { () -> Void in
                self.lblCredit.transform = CGAffineTransformMakeScale(1.0, 1.0)
            })
            }, completion: { (result) -> Void in
                
        })
    }
    
    func isMe(other: PlayerProtocolObserver) -> Bool {
        return self.isEqual(other)
    }
}

extension ScoreView : PhaseProgressDelegate {
    func updateProgress(newProgress: Double) {
        self.cpVProgress.progress = newProgress
        self.lblProgress.text = NSString(format: self.progress, Int(newProgress*100)) as String
        println(NSString(format: self.progress, newProgress*100))
    }
}