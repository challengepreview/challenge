//
//  GameViewController.swift
//  ChallengePreview
//
//  Created by Erick Ricardo Mattos on 06/07/15.
//  Copyright (c) 2015 Erick Ricardo Mattos. All rights reserved.
//

import UIKit

class GameViewController: UIViewController, UIPopoverPresentationControllerDelegate {
    
    let listCompleted = "listCompleteSegue"
    
    private var verticalDirectionSlaveCollectionView : Bool = false
    
    private var delegateDataSourceMaster:   MasterCollectionView!
    private var delegateDataSourceSlave:    SlaveCollectionView!
    private var movedImageSlave:            Bool                    = false
    private var largeImageView:             UIImageView!            = nil
            var bgf:                        UIView!
            var bg:                         UIImageView!
    private var menuKind:                   MenuKindItem!
    private var phaseDB: PhaseGame!
    var animationEnabled:Bool = true
    var scrollView:UIMenuScroll!
    var phaseName: Menu!{
        willSet(newPhase){
            if self.phaseDB != nil {
                fatalError("Jah tinha uma fase")
            }
        }
        didSet {
            self.getDados()
        }
    }
    
    private var lastCellMasterSelected: UICollectionViewCell! = nil /* feedback sobre celula master selecionada */
    private let colorBorderHighlighted: UIColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 153/255.0, alpha: 1)
    
    @IBOutlet weak var masterCollectionView: UICollectionView!
    @IBOutlet weak var slaveCollectionView: UICollectionView!
    @IBOutlet weak var viewAncora: UIView!
    @IBOutlet weak var scoreView: ScoreView!
    
    convenience init(){
        self.init(nibName: nil, bundle: nil)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        self.phaseDB = nil
        super.init(nibName: nibNameOrNil, bundle: nil)
    }

    required init(coder aDecoder: NSCoder) {
        self.phaseDB = nil
        super.init(coder: aDecoder)
    }
    
    deinit {
        println("GameViewController deinitialized")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuKind = MenuKindItem(menu: phaseName)
        bg = UIImageView()
        bg.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height)
        bg.layer.zPosition = -2
        self.view.addSubview(bg)
        bgf = UIView(frame: CGRectMake(0, 0, self.view.frame.width, self.view.frame.height))
        bgf.alpha = 0.0
        bgf.backgroundColor = UIColor.whiteColor()
        bgf.layer.zPosition = -1
        bg.addSubview(bgf)
        /* set os delegate e o dataSource da collection view master */
        self.masterCollectionView!.delegate      = self.delegateDataSourceMaster
        self.masterCollectionView!.dataSource    = self.delegateDataSourceMaster
        
        /* set os delegate e o dataSource da collection view slave */
        self.slaveCollectionView!.delegate      = self.delegateDataSourceSlave
        self.slaveCollectionView!.dataSource    = self.delegateDataSourceSlave
        
        let flowLayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        if self.verticalDirectionSlaveCollectionView {
            flowLayout.scrollDirection = .Vertical
        }else{
            flowLayout.scrollDirection = .Horizontal
        }
        self.slaveCollectionView.collectionViewLayout = flowLayout
        
        // animações
        self.slaveCollectionView.alpha = 0
        self.masterCollectionView.alpha = 0
        self.masterCollectionView.layer.zPosition = 1
        self.slaveCollectionView.layer.zPosition = 1
        
        // scrollView
        self.scrollView = UIMenuScroll(frame: CGRectNull)
        self.scrollView.layer.zPosition = 1
        self.scrollView.backgroundColor = UIColor.clearColor()
        //let gestureRecognizer = UITapGestureRecognizer(target: self, action: "handlePan:")
        //self.scrollView.addGestureRecognizer(gestureRecognizer)
        self.scrollView.collection = self.slaveCollectionView
        self.scrollView.orientation = 1
        self.scrollView.parentClass = self
        self.view.addSubview(self.scrollView)
        
        
        self.scoreView.setDelegate(self)
        self.phaseDB.setProgressDelegate(scoreView)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.slaveCollectionView.alpha = 1.0
            self.masterCollectionView.alpha = 1.0
            self.bgf.alpha = 0.5
        })
        self.verificaCompletude()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.animationEnabled = false
        self.scrollView.acoxambrar()
        self.animationEnabled = true
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.masterCollectionView.reloadData()
        self.slaveCollectionView.reloadData()
        bg.image = menuKind.thumbnail
        bg.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height)
        bgf.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height)
    }
    
    
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        var tipView = self.view.viewWithTag(127) as? UIVisualEffectView
        if (tipView != nil) {
            //println(tipCard)
            tipView?.frame = self.view.frame
            var tipCard = tipView?.viewWithTag(128) as? TipCard
            tipCard?.layoutSubviews()
            //tipCard?.layoutIfNeeded()
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.scrollView.frame = self.slaveCollectionView.frame

    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == self.listCompleted{
            let helpVC = segue.destinationViewController as! HelpViewController
            helpVC.setPhaseMaster(self.phaseDB, masterName: self.masterNameDetailCompleted)
            segue.destinationViewController.popoverPresentationController!!.delegate = self
        }
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController!, traitCollection: UITraitCollection!) -> UIModalPresentationStyle {
        return .None
    }
    
    private var masterNameDetailCompleted: String!
    private var subLayerBorderSelected: [CALayer] = [CALayer]()
    
    func setMasterName(name: String){
        self.masterNameDetailCompleted = name
    }
    
    func removeImageMaster(master: String){
        self.delegateDataSourceMaster.removeImageMaster(master)
        self.masterCollectionView.reloadData()
    }
    
    func exit(){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    var viewWin: WinCard!
    
    func verificaCompletude(){
        if self.slaveCollectionView.numberOfItemsInSection(0) == 0 {/* numero de celulas eh zero, logo todas jah estão completas */
            viewWin = WinCard(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
            viewWin.layer.zPosition = 3
            viewWin.setDelegate(self)
            
            self.view.addSubview(viewWin)
            
            viewWin.setTranslatesAutoresizingMaskIntoConstraints(false)
            
            let winTop        = NSLayoutConstraint(item: viewWin, attribute: .Top      , relatedBy: .Equal, toItem: self.slaveCollectionView, attribute: .Top,       multiplier:   1, constant: 0)
            let winBottom     = NSLayoutConstraint(item: viewWin, attribute: .Bottom   , relatedBy: .Equal, toItem: self.view, attribute: .Bottom,    multiplier:   1, constant: 0)
            let winLeading    = NSLayoutConstraint(item: viewWin, attribute: .Leading  , relatedBy: .Equal, toItem: self.slaveCollectionView, attribute: .Leading,   multiplier:   1, constant: 0)
            let winTrailing   = NSLayoutConstraint(item: viewWin, attribute: .Trailing , relatedBy: .Equal, toItem: self.slaveCollectionView, attribute: .Trailing,  multiplier:   1, constant: 0)
            
            self.view.addConstraint(winTop)
            self.view.addConstraint(winBottom)
            self.view.addConstraint(winLeading)
            self.view.addConstraint(winTrailing)
            
            viewWin.setupView()
        }
    }
    /* inWho eh a view que ganhara a borda; howMany eh o numero de segmentos de bordas por lado seguindo: horizontal e vertical; which colors eh qnts cores serao usadas -> cuidado para nao ficar com cores repetidas, sugestao eh colocar uma cor para cada segmento usando a ordem: top, right, buttom, left no sentido horario; width eh a espessura da borda */
    func addFunBorder(inWho view: UIView, howMany quantity: [Int], whichColors colors: [UIColor], width: CGFloat){
        if quantity.count < 2{
            fatalError("Uma view tem bordas horizontais e verticas, sao necessario pelo menos 2 valores em howMany")
        }
        /* tamanho de cada segmento horizontal, X, e vertical ,Y.  */
        var medidaX: CGFloat = view.frame.width  / CGFloat(quantity[0])
        var medidaY: CGFloat = (view.frame.height - 2*width) / CGFloat(quantity[1])
        /* bordas em Top e Bottom */
        for i in (0...quantity[0]-1){
            var borderTopi = CALayer()
            var borderButtomi = CALayer()
            
            borderTopi.frame    = CGRect(x: CGFloat(i)*medidaX, y: 0, width: medidaX, height: width)
            borderButtomi.frame = CGRect(x: CGFloat(i)*medidaX, y: view.frame.height - width, width: medidaX, height: width)
            
            borderTopi.backgroundColor    = colors[i%colors.count].CGColor
            borderButtomi.backgroundColor = colors[(2*quantity[0] + quantity[1] - (i + 1) )%colors.count].CGColor
            
            view.layer.addSublayer(borderTopi)
            view.layer.addSublayer(borderButtomi)
            
            self.subLayerBorderSelected.append(borderTopi)
            self.subLayerBorderSelected.append(borderButtomi)
        }
        
        for j in (0...quantity[1]-1){
            var borderLeftj = CALayer()
            var borderRightj = CALayer()
            
            borderRightj.frame    = CGRect(x: view.frame.width - width, y: CGFloat(j)*medidaY+width, width: width, height: medidaY)
            borderLeftj.frame = CGRect(x: 0, y: CGFloat(j)*medidaY+width, width: width, height: medidaY)
            
            borderRightj.backgroundColor = colors[( quantity[0] + j + 1 )%colors.count].CGColor
            borderLeftj.backgroundColor = colors[( 2*quantity[0] + 2*quantity[1] - j )%colors.count].CGColor
            
            view.layer.addSublayer(borderLeftj)
            view.layer.addSublayer(borderRightj)
            
            
            self.subLayerBorderSelected.append(borderLeftj)
            self.subLayerBorderSelected.append(borderRightj)
        }
    }
    
    /* Diminuir imagem */
    func getNewScaleImageDragByDistance(now: CGPoint) -> CGFloat{
        let origin = self.view.convertPoint(self.slaveCollectionView.center, fromView: self.slaveCollectionView)
        let end = self.view.convertPoint(self.masterCollectionView.center, fromView: self.masterCollectionView)
        let heightMax = self.masterCollectionView.frame.height
        let heightIni = self.slaveCollectionView.frame.height
    
        /* se esta abaixo da imagem de origem na vertical ignoro e mantenho a escala */
        if origin.y <= now.y {
            return 1
        }
        
        var proportionalHeight =  ( heightMax ) / heightIni
        
        if now.y <= end.y {
            return proportionalHeight
        }
        
        var m = (1 - proportionalHeight ) / ( origin.y - end.y )
        
        return 1 + m * ( now.y - origin.y )
    }
    
    func flashView(color: UIColor){
        UIView.animateKeyframesWithDuration(2.0, delay: 0, options: nil, animations: { () -> Void in
            UIView.addKeyframeWithRelativeStartTime(0, relativeDuration: 0.3, animations: { () -> Void in
                self.bgf.backgroundColor = color
            })
            UIView.addKeyframeWithRelativeStartTime(0.3, relativeDuration: 0.2, animations: { () -> Void in
                self.bgf.backgroundColor = UIColor.whiteColor()
            })
            UIView.addKeyframeWithRelativeStartTime(0.5, relativeDuration: 0.3, animations: { () -> Void in
                self.bgf.backgroundColor = color
            })
            UIView.addKeyframeWithRelativeStartTime(0.8, relativeDuration: 0.2, animations: { () -> Void in
                self.bgf.backgroundColor = UIColor.whiteColor()
            })
            }, completion: { (result) -> Void in
        })
    }
    
    private func getDados(){
        self.phaseDB = PhaseGameImplementation(phaseMenu: phaseName)
        self.delegateDataSourceMaster = MasterCollectionView(imagesName: self.phaseDB.getMasterImagesName(), who: self)
        self.delegateDataSourceSlave = SlaveCollectionView(imagesName: self.phaseDB.getSlaveImagesName(), who: self)
    }
}

extension GameViewController : CollectionCellDelegate {
    
    func touchBegan(point: CGPoint, withCell cell: UICollectionViewCell){
        
        let menuPhoto = (cell as! SlaveCell).myself
        
        if self.largeImageView == nil{
            self.largeImageView = UIImageView()
            self.largeImageView.alpha = 0.8
            self.largeImageView.layer.zPosition = 2
            self.view.addSubview(self.largeImageView)
            self.largeImageView.hidden = true
        }
        
        if self.largeImageView.hidden {
            self.largeImageView.image = menuPhoto.thumbnail
            
            var size = menuPhoto.thumbnail.size
            
            size.width *= (self.slaveCollectionView.frame.size.height * 0.8)/size.height
            size.height = (self.slaveCollectionView.frame.size.height * 0.8)
            
            self.largeImageView.frame.size = size
            
            self.largeImageView.layer.position = point
            
            //self.largeImageView.hidden = false
            self.movedImageSlave = true
            
            self.slaveCollectionView.scrollEnabled = false
        }
        /* para o scroll */
        self.delegateDataSourceSlave.directionScroll = .None
        self.delegateDataSourceMaster.directionScroll = .None
    }
    
    func touchMoved(point: CGPoint, withCell cell: UICollectionViewCell){
        /* para o scroll */
        self.delegateDataSourceSlave.directionScroll = .None
        self.delegateDataSourceMaster.directionScroll = .None
        
        if self.largeImageView == nil{
            self.largeImageView = UIImageView()
            self.view.addSubview(self.largeImageView)
        }
        
        if(animationEnabled){
            self.largeImageView.hidden = !self.movedImageSlave
        }
        
        if !self.largeImageView.hidden {
            self.largeImageView.layer.position = point
            
            var scaleIni = self.getNewScaleImageDragByDistance(point)
            
            UIView.animateWithDuration(0.3, animations: { () -> Void in
                self.largeImageView.transform = CGAffineTransformMakeScale(scaleIni, scaleIni)
            })
            
            if self.verticalDirectionSlaveCollectionView {
                if self.largeImageView.frame.origin.y + self.largeImageView.frame.size.height > self.slaveCollectionView.frame.origin.y + self.slaveCollectionView.frame.size.height {
                    /* para baixo */
                    self.delegateDataSourceSlave.directionScroll = .Down
                    
                    self.delegateDataSourceSlave.scrollAutomaticALitBit()
                }else if (self.slaveCollectionView.frame.origin.y - self.largeImageView.layer.position.y<50)&&(CGRectContainsPoint(self.slaveCollectionView.frame, point)) {
                    self.delegateDataSourceSlave.directionScroll = .Up
                
                    self.delegateDataSourceSlave.scrollAutomaticALitBit()
                }
            }else{
                if self.largeImageView.layer.position.x < 50 && CGRectContainsPoint(self.slaveCollectionView.frame, point)
                {
                    /* para esquerda */
                    self.delegateDataSourceSlave.directionScroll = .Left
                    
                    self.delegateDataSourceSlave.scrollAutomaticALitBit()
                }else if self.largeImageView.layer.position.x > self.view.frame.size.width - 50 && CGRectContainsPoint(self.slaveCollectionView.frame, point) {
                    self.delegateDataSourceSlave.directionScroll = .Right
                    
                    self.delegateDataSourceSlave.scrollAutomaticALitBit()
                }
            }
            
            if self.largeImageView.layer.position.x < 50 && CGRectContainsPoint(self.masterCollectionView.frame, point)
                 {
                /* para esquerda */
                self.delegateDataSourceMaster.directionScroll = .Left
                
                self.delegateDataSourceMaster.scrollAutomaticALitBit()
            }else if self.largeImageView.layer.position.x > self.view.frame.size.width - 50 && CGRectContainsPoint(self.masterCollectionView.frame, point) {
                self.delegateDataSourceMaster.directionScroll = .Right
                
                self.delegateDataSourceMaster.scrollAutomaticALitBit()
            }
            
            /* feedback sobre celula escolhida */
            if self.lastCellMasterSelected != nil {
                /*self.subLayerBorderSelected.map({$0.removeFromSuperlayer()})
                self.subLayerBorderSelected.removeAll(keepCapacity: true)
                self.lastCellMasterSelected = nil
                
                self.lastCellMasterSelected.layer.borderWidth = 0.0
                self.lastCellMasterSelected.layer.borderColor = UIColor.clearColor().CGColor
                self.lastCellMasterSelected = nil
                */
                self.lastCellMasterSelected.layer.borderWidth = 0.0
                self.lastCellMasterSelected.layer.borderColor = UIColor.clearColor().CGColor
                self.lastCellMasterSelected = nil
            }
            
            if self.masterCollectionView.frame.contains(point){
                if let ip = self.masterCollectionView.indexPathForItemAtPoint( self.masterCollectionView.convertPoint(point, fromView: cell.superview?.superview) ){
                    if let cellMaster = self.masterCollectionView.cellForItemAtIndexPath(ip){
                        self.lastCellMasterSelected = cellMaster
                        /*self.addFunBorder(inWho: self.lastCellMasterSelected, howMany: [3,3], whichColors: [UIColor.greenColor(), UIColor.yellowColor(), UIColor.redColor()], width: 10.0)*/
                        self.lastCellMasterSelected.layer.borderWidth = 10.0
                        self.lastCellMasterSelected.layer.borderColor = self.colorBorderHighlighted.CGColor
                    }
                }
            }
        }
    }
    
    func touchEnded(point: CGPoint, withCell cell: UICollectionViewCell){
        let cellSlave: SlaveCell = cell as! SlaveCell
        /* para o scroll */
        self.delegateDataSourceSlave.directionScroll = .None
        self.delegateDataSourceMaster.directionScroll = .None
        
        if !self.largeImageView.hidden {
            if let indexPath = self.masterCollectionView.indexPathForItemAtPoint(self.masterCollectionView.convertPoint(point, fromView: cell.superview?.superview))
            {
                if let cellMaster: MasterCell? = self.masterCollectionView.cellForItemAtIndexPath(indexPath) as? MasterCell {
                    if self.phaseDB.isRelated(cellMaster!.myself.imageName, imageSlave: cellSlave.myself.imageName) {
                        if let indexPathSlave = self.slaveCollectionView.indexPathForCell(cell){
                            
                            //masterCollectionView.scrollToItemAtIndexPath(indexPath, atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: true)
                            
                            SoundController.sharedInstance.playRightSound()
                            UIView.animateWithDuration(1, animations: { () -> Void in
                                var theFrame = self.largeImageView.frame
                                theFrame.origin = self.masterCollectionView.convertPoint(cellMaster!.center, toView: self.largeImageView.superview)
                                theFrame.size = CGSize(width: 1, height: 1)
                                self.largeImageView.frame = theFrame
                                }, completion: { (result) -> Void in
                                    self.largeImageView.transform = CGAffineTransformMakeScale(1, 1)
                                    self.largeImageView.hidden = true
                            })
                            
                            self.delegateDataSourceSlave.removeImageForIndex(indexPathSlave.row)
                            self.slaveCollectionView.reloadData()
                            self.flashView(UIColor.greenColor())

                            self.verificaCompletude()
                        }
                    }else{
                        SoundController.sharedInstance.playWrongSound()
                        UIView.animateWithDuration(0.5, animations: { () -> Void in
                            var theFrame = self.largeImageView.frame
                            theFrame.origin = self.slaveCollectionView.convertPoint(cellSlave.center, toView: self.largeImageView.superview)
                            theFrame.size = CGSize(width: 1, height: 1)
                            self.largeImageView.frame = theFrame
                            }, completion: { (result) -> Void in
                                self.largeImageView.hidden = true
                                self.view.backgroundColor = UIColor.clearColor()
                                self.largeImageView.transform = CGAffineTransformMakeScale(1, 1)
                        })
                        
                        /* picar a tela como modo de dizer que errou */
                        self.flashView(UIColor.redColor())
                    }
                }
                else{
                    self.largeImageView.hidden = true
                    self.largeImageView.transform = CGAffineTransformMakeScale(1, 1)
                }
            }
            else{
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    var theFrame = self.largeImageView.frame
                    theFrame.origin = self.slaveCollectionView.convertPoint(cellSlave.center, toView: self.largeImageView.superview)
                    theFrame.size = CGSize(width: 1, height: 1)
                    self.largeImageView.frame = theFrame
                    }, completion: { (result) -> Void in
                        self.largeImageView.transform = CGAffineTransformMakeScale(1, 1)
                        self.largeImageView.hidden = true
                })
            }
            
            self.movedImageSlave = false
            //self.largeImageView.hidden = true
            self.slaveCollectionView.scrollEnabled = true
        }
        
        /* feedback para celula selecionada */
        if self.lastCellMasterSelected != nil {/*
            self.subLayerBorderSelected.map({$0.removeFromSuperlayer()})
            self.subLayerBorderSelected.removeAll(keepCapacity: true)
            self.lastCellMasterSelected = nil*/
            self.lastCellMasterSelected.layer.borderWidth = 0.0
            self.lastCellMasterSelected.layer.borderColor = UIColor.clearColor().CGColor
            self.lastCellMasterSelected = nil
        }
    }
}

extension GameViewController : TipCardDelegate {
    func tipCardWillSlide(tipCard: TipCard){
        if self.phaseDB.shouldShowTip(tipCard.imageName){
            tipCard.previewTip      = self.phaseDB.tryShowTip(tipCard.imageName)
            tipCard.tipShowedBefore = true
            return
        }
        
        let cancellMessage = NSLocalizedString("Cancelar", comment: "Opcao de nao gastar dinheiro para uma ajuda")
        let okMessage = NSLocalizedString("OK", comment: "Opcao para continuar e tentar gastar dinheiro para uma ajuda")
        let message = NSLocalizedString("São necessários %d créditos.", comment: "Opcao de nao gastar dinheiro para uma ajuda")
        let titleMessage = NSLocalizedString("Gasto para liberar a dica.", comment: "Titulo para alerta que confirma a necessidade de gastar credito")
        
        let custHelp: Int = self.phaseDB.getCustForHelpTip()
        let refreshAlert = UIAlertController(title: titleMessage, message: NSString(format: message, custHelp) as String, preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: cancellMessage, style: .Default, handler: { (action: UIAlertAction!) in
            return
        }))
        
        refreshAlert.addAction(UIAlertAction(title: okMessage, style: .Default, handler: { (action: UIAlertAction!) in
            let maybeTip = self.phaseDB.tryShowTip(tipCard.imageName)
            if maybeTip != "" || self.phaseDB.requestMoreCredit(custHelp){
                tipCard.previewTip      = maybeTip
                tipCard.tipShowedBefore = true
                tipCard.flipImageTipCard()
                return
            }
        }))
        presentViewController(refreshAlert, animated: true, completion: nil)
        
        return
    }
}

extension GameViewController: ScoreViewDelegate{
    func buttomTaped() {
        self.exit()
    }
}

extension GameViewController: WinCardDelegate{
    func willExit() {
        self.exit()
    }
    
    func shouldReset() {
        phaseDB.reset()
        
        self.delegateDataSourceMaster.resetLevel(self.phaseDB.getMasterImagesName())
        self.delegateDataSourceSlave.resetLevel(self.phaseDB.getSlaveImagesName())
        
        self.slaveCollectionView.reloadData()
        self.masterCollectionView.reloadData()
        
        if viewWin != nil {
            viewWin.removeFromSuperview()
        }
    }
}

