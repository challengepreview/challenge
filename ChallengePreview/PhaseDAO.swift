//
//  ChallengeDAO.swift
//  MiniChallengeTracker
//
//  Created by Fernando Celarino on 6/23/15.
//  Copyright (c) 2015 Fernando Celarino. All rights reserved.
//

import Foundation
import CoreData

class PhaseDAO
{
    static func findPhase(menu:Menu, master:String, dependent:String) -> Phase!
    {
        // creating fetch request
        let request = NSFetchRequest(entityName: "Phase")
        
        // assign predicate
        request.predicate = NSPredicate(format: "phasesMenu == %@ AND master == %@ AND dependent == %@", menu, master, dependent)
        
        // perform search
        var error:NSErrorPointer = nil
        let results:[Phase] = DatabaseManager.sharedInstance.managedObjectContext?.executeFetchRequest(request, error: error) as! [Phase]
        
        if results.count > 0{
            return results[0]
        }
        
        return nil
    }
    
    static func findAllForPhase(faseMenu:Menu) -> [Phase]
    {
        // creating fetch request
        let request = NSFetchRequest(entityName: "Phase")
        
        // assign predicate
        request.predicate = NSPredicate(format: "phasesMenu == %@", faseMenu)
        
        // perform search
        var error:NSErrorPointer = nil
        let results:[Phase] = DatabaseManager.sharedInstance.managedObjectContext?.executeFetchRequest(request, error: error) as! [Phase]
        
        return results
    }
    
    static func increseAttemptsPhase(menu:Menu, master:String, dependent:String){
        let error:NSErrorPointer = nil
        
        let phase = findPhase(menu, master: master, dependent: NSLocalizedString(dependent, comment: ""))
        
        phase.setValue(NSNumber(integer: phase.attempts.integerValue+1), forKey: "attempts")
        
        DatabaseManager.sharedInstance.managedObjectContext?.save(error)
        
        if (error != nil)
        {
            // log error
            print(error)
        }
    }
    
    static func completeImagePhase(menu:Menu, master:String, dependent:String){
        let error:NSErrorPointer = nil
        let phaseImage: Phase = findPhase(menu, master: master, dependent: NSLocalizedString(dependent, comment: ""))
        
        phaseImage.setValue(NSNumber(bool: true), forKey: "completed")
        
        DatabaseManager.sharedInstance.managedObjectContext?.save(error)
        
        if (error != nil)
        {
            // log error
            print(error)
        }
    }
    
    static func canReleaseTip(menu:Menu, master:String, dependent:String) -> String {
        let c_x = PlayerDAO.calculateTakeTip()
        if c_x <= PlayerDAO.getCredit() {
            let tip = PhaseDAO.findPhase(menu, master: master, dependent: NSLocalizedString(dependent, comment: ""))
            if !tip.showedTip.boolValue {
                PlayerDAO.takeCX(c_x)
                
                let error:NSErrorPointer = nil
                tip.setValue(NSNumber(bool: true), forKey: "showedTip")
                
                DatabaseManager.sharedInstance.managedObjectContext?.save(error)
                
                if (error != nil)
                {
                    // log error
                    print(error)
                }
            }
            return tip.tip
        }
        return ""
    }
    
    static func removeMasterFromList(menu:Menu, master:String, reallyRemove: Bool) -> Bool {
        var error:NSErrorPointer = nil
        let c_x = PlayerDAO.calculateTakeX(menu.numberMasterReleased.integerValue + 1, A: PlayerDAO.AOthers)
    
        if c_x <= PlayerDAO.getCredit() {
            
            PlayerDAO.takeCX(c_x)
            // creating fetch request
            if reallyRemove{
                let request = NSFetchRequest(entityName: "Phase")
                
                // assign predicate
                request.predicate = NSPredicate(format: "phasesMenu == %@ AND master = %@", menu, master)
                
                // perform search
                let results:[Phase] = DatabaseManager.sharedInstance.managedObjectContext?.executeFetchRequest(request, error: error) as! [Phase]
                
                for r in results{
                    r.setValue(NSNumber(bool: false), forKey: "showMaster")
                }
            }
            
            menu.setValue(NSNumber(integer: menu.numberMasterReleased.integerValue + 1), forKey: "numberMasterReleased")
            
            DatabaseManager.sharedInstance.managedObjectContext?.save(error)
            
            if (error != nil)
            {
                // log error
                print(error)
            }
            
            return true
        }
        return false
    }
    
    static func shouldShowCompletedImageListForMaster(menu:Menu, master:String) -> Bool{
        let request = NSFetchRequest(entityName: "Phase")
        
        // assign predicate
        request.predicate = NSPredicate(format: "phasesMenu == %@ AND master = %@ AND showCompleted = %@", menu, master, NSNumber(bool: true))
        
        // perform search
        var error:NSErrorPointer = nil
        let results:[Phase] = DatabaseManager.sharedInstance.managedObjectContext?.executeFetchRequest(request, error: error) as! [Phase]
        
        if (error != nil)
        {
            // log error
            print(error)
        }
        
        return results.count > 0
    }
    
    static func showCompletedImageListForMaster(menu:Menu, master:String) -> Bool{
        
        let request = NSFetchRequest(entityName: "Phase")
        
        // assign predicate
        request.predicate = NSPredicate(format: "phasesMenu == %@ AND master = %@", menu, master)
        
        // perform search
        var error:NSErrorPointer = nil
        let results:[Phase] = DatabaseManager.sharedInstance.managedObjectContext?.executeFetchRequest(request, error: error) as! [Phase]
        
        if (error != nil)
        {
            // log error
            print(error)
        }
        
        if !results[0].showCompleted.boolValue {
            let c_x = PlayerDAO.custShowCompletedImageListForMaster
            
            if c_x <= PlayerDAO.getCredit() {
                PlayerDAO.takeCX(c_x)
                
                for r in results{
                    r.setValue(NSNumber(bool: true), forKey: "showCompleted")
                }
                
                DatabaseManager.sharedInstance.managedObjectContext?.save(error)
                
                if (error != nil)
                {
                    // log error
                    print(error)
                }
                return true
            }
            
            return false
        }else{
            return true
        }
    }
    
    static func insert(objectToBeInserted:Phase)
    {
        // insert element into context
        DatabaseManager.sharedInstance.managedObjectContext?.insertObject(objectToBeInserted)
        
        // save context
        var error:NSErrorPointer = nil
        DatabaseManager.sharedInstance.managedObjectContext?.save(error)
        if (error != nil)
        {
            // log error
            print(error)
        }
    }
    
    static func delete(objectToBeDeleted:Phase)
    {
        // remove object from context
        var error:NSErrorPointer = nil
        DatabaseManager.sharedInstance.managedObjectContext?.deleteObject(objectToBeDeleted)
        DatabaseManager.sharedInstance.managedObjectContext?.save(error)
        
        // log error
        if (error != nil)
        {
            // log error
            print(error)
        }
    }
    
    static func resetPhase(menu: Menu){
        for key in menu.menuPhases{
            let phaseMenu: Phase = key as! Phase
            phaseMenu.setValue(NSNumber(bool: true), forKey: "showMaster")
            phaseMenu.setValue(NSNumber(bool: false), forKey: "countable")
            phaseMenu.setValue(NSNumber(bool: false), forKey: "completed")
        }
        
        var error:NSErrorPointer = nil
        DatabaseManager.sharedInstance.managedObjectContext?.save(error)
    }
    
    
}