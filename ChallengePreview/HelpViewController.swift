//
//  HelpViewController.swift
//  ChallengePreview
//
//  Created by Erick Ricardo Mattos on 15/07/15.
//  Copyright (c) 2015 Erick Ricardo Mattos. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController {
    private var phaseDB: PhaseGame!
    private var masterImageName: String!
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet weak var exibeCompletos: UIButton!
    
    private let reuseIdentifier = "completedCell"
    private var imagesCompleted: [(name: String, description: String?)] = []
    
    private let cancellMessage = NSLocalizedString("Cancelar", comment: "Opcao de nao gastar dinheiro para uma ajuda")
    private let okMessage = NSLocalizedString("OK", comment: "Opcao para continuar e tentar gastar dinheiro para uma ajuda")
    private let message = NSLocalizedString("São necessários %d créditos.", comment: "Opcao de nao gastar dinheiro para uma ajuda")
    private let titleMessage = NSLocalizedString("Gasto com ajuda.", comment: "Titulo para alerta que confirma a necessidade de gastar credito")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imagesCompleted = self.phaseDB.getCompletedSlaveForMaster(self.masterImageName)
        if self.phaseDB.shouldShowCompletedSlaveMaster(self.masterImageName){
            exibeCompletos.hidden = true
        }
    }
    
    @IBAction func eliminarBandeira(sender: AnyObject) {
        let custHelp: Int = self.phaseDB.getCustForHelpX()
        let refreshAlert = UIAlertController(title: self.titleMessage, message: NSString(format: message, custHelp) as String, preferredStyle: UIAlertControllerStyle.Alert)
        refreshAlert.addAction(UIAlertAction(title: cancellMessage, style: .Default, handler: { (action: UIAlertAction!) in
            return
        }))
        refreshAlert.addAction(UIAlertAction(title: okMessage, style: .Default, handler: { (action: UIAlertAction!) in
            let itsPossible = self.phaseDB.removeMaster(self.masterImageName)
            if itsPossible || self.phaseDB.requestMoreCredit(custHelp){
                if self.phaseDB.completedMaster(self.masterImageName){
                    var tmpController :GameViewController! = self.presentingViewController as! GameViewController
                    self.dismissViewControllerAnimated(true, completion: nil)
                    
                    tmpController.removeImageMaster(self.masterImageName)
                }
            }
        }))
        presentViewController(refreshAlert, animated: true, completion: nil)
    }
    
    @IBAction func exibirCompletos(sender: AnyObject) {
        let custHelp: Int = PlayerDAO.custShowCompletedImageListForMaster
        let refreshAlert = UIAlertController(title: self.titleMessage, message: NSString(format: message, custHelp) as String, preferredStyle: UIAlertControllerStyle.Alert)
        refreshAlert.addAction(UIAlertAction(title: cancellMessage, style: .Default, handler: { (action: UIAlertAction!) in
            return
        }))
        refreshAlert.addAction(UIAlertAction(title: okMessage, style: .Default, handler: { (action: UIAlertAction!) in
            let itsPossible = self.phaseDB.showCompletedSlaveMaster(self.masterImageName)
            if itsPossible || self.phaseDB.requestMoreCredit(custHelp){
                /* faz algo */
                self.imagesCompleted = self.phaseDB.getCompletedSlaveForMaster(self.masterImageName)
                self.exibeCompletos.hidden = true
                self.tableView.reloadData()
            }
        }))
        presentViewController(refreshAlert, animated: true, completion: nil)
    }
    
    func setPhaseMaster(phase: PhaseGame, masterName: String){
        if self.phaseDB != nil || self.masterImageName != nil {
            fatalError("Jah foi setado a fase e o mestre")
        }
        
        self.phaseDB = phase
        self.masterImageName = masterName
    }
}

extension HelpViewController: UITableViewDelegate{
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
}

extension HelpViewController: UITableViewDataSource{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.imagesCompleted.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(self.reuseIdentifier, forIndexPath: indexPath) as! HelpTableViewCell
        
        cell.label.text = self.imagesCompleted[indexPath.row].description
        cell.myImage.backgroundColor = UIColor.blackColor()
        let imageCell: UIImage = UIImage(named: self.imagesCompleted[indexPath.row].name)!
        var size = imageCell.size
        
        size.width *= (cell.frame.size.height * 0.9)/size.height
        size.height = (cell.frame.size.height * 0.9)
        
        cell.heightConstraint.constant  = size.height
        cell.widthConstraint.constant   = size.width
        cell.myImage.image = imageCell
        
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = UIColor.yellowColor()
        }else{
            cell.backgroundColor = UIColor.blueColor()
        }
        return cell
    }
}

