//
//  PhaseProtocol.swift
//  ChallengePreview
//
//  Created by Erick Ricardo Mattos on 07/07/15.
//  Copyright (c) 2015 Erick Ricardo Mattos. All rights reserved.
//

import Foundation

protocol PhaseGame {
    init(phaseMenu:Menu)
    
    func getMasterImagesName()  -> [String]
    func getSlaveImagesName()   -> [(name: String, tip: String?)]
    func isRelated(imageMaster: String, imageSlave: String) -> Bool
    func getLocalScore()    -> Int
    func getGlobalScore()   -> Int
    func removeMaster(master: String) -> Bool
    func showCompletedSlaveMaster(master: String) -> Bool
    func shouldShowCompletedSlaveMaster(master: String) -> Bool
    func shouldShowTip(slave: String) -> Bool
    func tryShowTip(slave: String) -> String
    func getCompletedSlaveForMaster(master: String) -> [(name: String, description: String?)]
    func completedMaster(master: String) -> Bool
    func getCustForHelpX() -> Int
    func getCustForHelpTip() -> Int
    func requestMoreCredit(quantityFinal: Int) -> Bool
    func getProgress() -> Double
    func setProgressDelegate(delegate : PhaseProgressDelegate)
    func reset()
}
