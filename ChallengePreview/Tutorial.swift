//
//  Tutorial.swift
//  LinkImg
//
//  Created by Antonio Carlos Guimarães Junior on 8/5/15.
//  Copyright (c) 2015 Erick Ricardo Mattos. All rights reserved.
//

import Foundation

class Tutorial: NSObject {
    private var currentView: UIView!
    private var tutorialButton: UIImageView!

    init(view: UIView){
        super.init()
        
        
        // self.view from view controller
        self.currentView = view
        let tam = currentView.frame.width < currentView.frame.height ? 0.1 * currentView.frame.width : 0.1 * currentView.frame.height

        
        self.tutorialButton = UIImageView(frame: CGRectMake(0.1*tam, currentView.frame.height - 0.9*tam, 0.8*tam, 0.8*tam))
        self.tutorialButton.image = UIImage(named: "help")
        self.tutorialButton.userInteractionEnabled = true
        self.tutorialButton.layer.zPosition = 4
        self.tutorialButton.contentMode = .ScaleAspectFit
        
        currentView.addSubview(self.tutorialButton)

        
    }
    
    func showHelp(gestureRecognizer: UITapGestureRecognizer){
        println("help")
    }
    
    func setup(){
        let gestureRec = UITapGestureRecognizer(target: self, action: "showHelp:")
        tutorialButton.addGestureRecognizer(gestureRec)
        
        
    }
    
    func reshape(){
        let tam = currentView.frame.width < currentView.frame.height ? 0.1 * currentView.frame.width : 0.1 * currentView.frame.height
        tutorialButton.frame = CGRectMake(0.1*tam, currentView.frame.height - 0.9*tam, 0.8*tam, 0.8*tam)
    }
}