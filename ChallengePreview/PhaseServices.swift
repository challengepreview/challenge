//
//  PhaseService.swift
//  AssociationDBCreate
//
//  Created by José Ernesto Stelzer Monar on 06/07/15.
//  Copyright (c) 2015 José Ernesto Stelzer Monar. All rights reserved.
//

import Foundation

class PhaseServices
{
    static func createPhase(master:String, dependent:String, tip:String, masterDescription: String, phaseMenu: Menu) -> Phase
    {
        var phase:Phase = Phase()
        phase.master = master
        phase.dependent = dependent
        phase.tip = tip
        phase.masterDescription = masterDescription
        phase.attempts = 0
        phase.phasesMenu = phaseMenu
        phase.completed = NSNumber(bool: false)
        phase.showedTip = NSNumber(bool: false)
        phase.showCompleted = NSNumber(bool: false)
        phase.showMaster = NSNumber(bool: true)
        phase.countable = NSNumber(bool: true)
        // insert it
        PhaseDAO.insert(phase)
        
        return phase
    }
    
    static func createPhase(master:String, dependent:String, tip:String, phaseMenu: Menu) -> Phase{
        return PhaseServices.createPhase(master, dependent: dependent, tip: tip, masterDescription: master, phaseMenu: phaseMenu)
    }
    
    static func deleteGroupByTriplet(menu: Menu, master : String, dependent : String)
    {
        // create queue
        var auxiliarQueue:NSOperationQueue = NSOperationQueue()
        
        // create operation
        let deleteOperation : NSBlockOperation = NSBlockOperation(block: {
            // find challenge
            var phase:Phase? = PhaseDAO.findPhase(menu, master: master, dependent: dependent)
            if (phase != nil)
            {
                // delete challenge
                PhaseDAO.delete(phase!)
            }
        })
        
        // execute operation
        auxiliarQueue.addOperation(deleteOperation)
    }
    
}
