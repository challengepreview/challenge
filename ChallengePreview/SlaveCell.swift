//
//  SlaveCell.swift
//  ChallengePreview
//
//  Created by Erick Ricardo Mattos on 06/07/15.
//  Copyright (c) 2015 Erick Ricardo Mattos. All rights reserved.
//

import UIKit

class SlaveCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    var ativo:Bool = false
    
    private var delegateTouch: CollectionCellDelegate!
    var myself: SlaveCellItem!
    
    func setDelegateTouch(#delegateTouch: CollectionCellDelegate ){
        self.delegateTouch = delegateTouch
    }
    
    func touchBegan(point: CGPoint){
        if (self.delegateTouch != nil) {
            self.delegateTouch.touchBegan(point, withCell: self)
        }
    }
    
    func touchMoved(point: CGPoint){
        if (self.delegateTouch != nil) {
            self.delegateTouch.touchMoved(point, withCell: self)
        }
    }
    
    func touchEnded(point:CGPoint){
        if (self.delegateTouch != nil) {
            self.delegateTouch.touchEnded(point, withCell: self)
        }
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        super.touchesBegan(touches, withEvent: event)
        
        if (self.delegateTouch != nil) {
            let touch: UITouch = touches.first as! UITouch
            let newPosition = touch.locationInView(self.superview?.superview)/* viewcontroller? */
            self.delegateTouch.touchBegan(newPosition, withCell: self)
        }
    }
    
    override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {
        super.touchesMoved(touches, withEvent: event)
        
        if (self.delegateTouch != nil) {
            let touch: UITouch = touches.first as! UITouch
            let newPosition = touch.locationInView(self.superview?.superview)/* viewcontroller? */
            self.delegateTouch.touchMoved(newPosition, withCell: self)
        }
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        super.touchesEnded(touches, withEvent: event)
        
        if (self.delegateTouch != nil) {
            let touch: UITouch = touches.first as! UITouch
            let newPosition = touch.locationInView(self.superview?.superview)/* viewcontroller? */
            self.delegateTouch.touchEnded(newPosition, withCell: self)
        }
    }
}
