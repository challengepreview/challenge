//
//  SlaveCollectionView.swift
//  ChallengePreview
//
//  Created by Erick Ricardo Mattos on 07/07/15.
//  Copyright (c) 2015 Erick Ricardo Mattos. All rights reserved.
//

import UIKit

extension Array {
    func shuffled() -> [T] {
        if count < 2 { return self }
        var list = self
        for i in 0..<(list.count - 1) {
            let j = Int(arc4random_uniform(UInt32(list.count - i))) + i
            swap(&list[i], &list[j])
        }
        return list
    }
    
    mutating func shuffle() {
        if count < 2 { return }
        for i in 0..<(count - 1) {
            let j = Int(arc4random_uniform(UInt32(count - i))) + i
            swap(&self[i], &self[j])
        }
    }
}

class SlaveCollectionView: NSObject {
    
    private let velocityAutoScroll:         CGFloat                 =  50
            var directionScroll:            Direction               = .None
    
    private let reuseIdentifier = "SlaveCell"
    
    private var imagesMaster:[SlaveCellItem] = [SlaveCellItem]()
    private var maxHeight: CGFloat = -10.0
    
    private var whoCreateMe: CollectionCellDelegate!
    
    func photoForIndexPath(indexPath: NSIndexPath) -> SlaveCellItem {
        return imagesMaster[indexPath.row]
    }
    
    private override init(){
        
    }
    
    init(imagesName: [(name: String, tip: String?)], who: CollectionCellDelegate ){
        super.init()
        self.whoCreateMe = who
        for image in imagesName {
            if image.tip != nil {
                self.imagesMaster.insert(SlaveCellItem(imageName: image.name, tip : image.tip!), atIndex: 0)
            }else{
                self.imagesMaster.insert(SlaveCellItem(imageName: image.name, tip : ""), atIndex: 0)
                println(image.name)
            }
        }
    }
    
    func resetLevel(imagesName: [(name: String, tip: String?)]){
        self.imagesMaster.removeAll(keepCapacity: false)
        for image in imagesName {
            if image.tip != nil {
                self.imagesMaster.insert(SlaveCellItem(imageName: image.name, tip : image.tip!), atIndex: 0)
            }else{
                self.imagesMaster.insert(SlaveCellItem(imageName: image.name, tip : ""), atIndex: 0)
                println(image.name)
            }
        }
    }
    
    func removeImageForIndex(index: Int){
        self.imagesMaster.removeAtIndex(index)
    }
    
    func scrollAutomaticALitBit(){
        if self.directionScroll == .Up || self.directionScroll == .Down{
            let collectionView: UICollectionView = (self.whoCreateMe as! GameViewController).slaveCollectionView
            
            let newYOffset = collectionView.contentOffset.y + (CGFloat(self.directionScroll.rawValue) * self.velocityAutoScroll)
            
            if (newYOffset < collectionView.contentSize.height - collectionView.frame.size.height)&&( newYOffset > 0 ){
                collectionView.setContentOffset(CGPoint(x: collectionView.contentOffset.x, y: newYOffset ), animated: true)
            }
        }else if self.directionScroll != .None {
            let collectionView: UICollectionView = (self.whoCreateMe as! GameViewController).slaveCollectionView
            
            let newXOffset = collectionView.contentOffset.x + (CGFloat(self.directionScroll.rawValue/2) * self.velocityAutoScroll)
            
            if (newXOffset < collectionView.contentSize.width - collectionView.frame.size.width)&&( newXOffset > 0 ){
                collectionView.setContentOffset(CGPoint(x: newXOffset, y: collectionView.contentOffset.y ), animated: true)
            }
        }
    }
}

extension SlaveCollectionView : UICollectionViewDelegateFlowLayout {
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        var blurFilter = CIFilter(name: "CIGaussianBlur")
        blurFilter.setDefaults()
        blurFilter.setValue(NSNumber(float: 2.0), forKey: "inputRadius")
        
        
        var blur = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        
        let viewBlur = UIVisualEffectView(effect: blur)
        viewBlur.layer.zPosition = 3
        viewBlur.frame = collectionView.superview!.frame
        viewBlur.userInteractionEnabled = true
        viewBlur.tag = 127
        
        var tipCard = TipCard(frame:CGRect(x: 25, y: 25, width: viewBlur.frame.width-50, height: viewBlur.frame.height-100))
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! SlaveCell
        
        tipCard.previewImage = UIImage(named: cell.myself.imageName)
        //tipCard.previewTip = cell.myself.tip
        tipCard.layer.zPosition = 4
        tipCard.layer.masksToBounds = true
        tipCard.layer.backgroundFilters = [blurFilter]
        tipCard.layer.cornerRadius = 10
        tipCard.tag = 128
        tipCard.delegate = self.whoCreateMe as! GameViewController
        tipCard.tipShowedBefore = cell.myself.tip.isEmpty ? false : true
        tipCard.imageName = cell.myself.imageName
        
        let tapGesture = UITapGestureRecognizer(target: self, action: "tipCardTap:")
        viewBlur.addGestureRecognizer(tapGesture)
        
        viewBlur.addSubview(tipCard)
        collectionView.superview!.addSubview(viewBlur)
    }
    
    
    func tipCardTap(recognizer: UITapGestureRecognizer) {
        recognizer.view?.removeFromSuperview()
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            let photo = photoForIndexPath(indexPath)
            if var imageMaster = photo.thumbnail {
                var size = imageMaster.size
                
                size.width *= (collectionView.frame.size.height * 0.8)/size.height
                size.height = (collectionView.frame.size.height * 0.8)
                
                
                if size.height > self.maxHeight {
                    self.maxHeight = size.height
                }
                
                return size
            }
            return CGSize(width: 100, height: 100)
    }
}

extension SlaveCollectionView : UICollectionViewDataSource {
    
    /* numero de itens no menu */
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    /* numero de imagens por menu */
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesMaster.count
    }
    
    /* configurar as celulas */
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! SlaveCell
        // Configure the cell
        let menuPhoto = photoForIndexPath(indexPath)
        
        cell.setDelegateTouch(delegateTouch: self.whoCreateMe)
        
        cell.myself = menuPhoto
        
        cell.imageView.image = menuPhoto.thumbnail
        
        //cell.layer.cornerRadius = cell.frame.size.width/2
        
        return cell
    }
    
}

extension SlaveCollectionView : UIScrollViewDelegate {
    func scrollViewDidScroll(scrollView: UIScrollView){
        self.scrollAutomaticALitBit()
    }
}