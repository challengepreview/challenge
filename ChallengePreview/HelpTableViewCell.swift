//
//  HelpTableViewCell.swift
//  ChallengePreview
//
//  Created by Erick Ricardo Mattos on 15/07/15.
//  Copyright (c) 2015 Erick Ricardo Mattos. All rights reserved.
//

import UIKit

class HelpTableViewCell: UITableViewCell {

    @IBOutlet weak var myImage: UIImageView!
    @IBOutlet weak var label:    UILabel!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
}
