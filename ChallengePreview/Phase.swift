//
//  Phase.swift
//  ChallengePreview
//
//  Created by Erick Ricardo Mattos on 13/07/15.
//  Copyright (c) 2015 Erick Ricardo Mattos. All rights reserved.
//

import Foundation
import CoreData

class Phase: NSManagedObject {

    @NSManaged var dependent: String
    @NSManaged var master: String
    @NSManaged var masterDescription: String
    @NSManaged var tip: String
    @NSManaged var completed: NSNumber
    @NSManaged var showedTip: NSNumber
    @NSManaged var attempts: NSNumber
    @NSManaged var showMaster: NSNumber
    @NSManaged var showCompleted: NSNumber
    @NSManaged var countable: NSNumber
    @NSManaged var phasesMenu: Menu

    convenience init()
    {
        // get context
        let context:NSManagedObjectContext = DatabaseManager.sharedInstance.managedObjectContext!
        
        // create entity description
        let entityDescription:NSEntityDescription? = NSEntityDescription.entityForName("Phase", inManagedObjectContext: context)
        
        // call super using
        self.init(entity: entityDescription!, insertIntoManagedObjectContext: context)
    }

}
