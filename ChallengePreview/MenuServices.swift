//
//  MenuServices.swift
//  ChallengePreview
//
//  Created by Erick Ricardo Mattos on 06/07/15.
//  Copyright (c) 2015 Erick Ricardo Mattos. All rights reserved.
//

import Foundation

class MenuServices
{
    static func createMenu(descriptionMenu:String, imageMenuL:String, imageMenuP:String) -> Menu
    {
        var menu:Menu = Menu()
        
        menu.descriptionMenu        = descriptionMenu
        menu.imageMenuL             = imageMenuL
        menu.imageMenuP             = imageMenuP
        menu.locked                 = NSNumber(bool: true)
        menu.numberMasterReleased   = NSNumber(integer: 0)
        
        // insert it
        MenuDAO.insert(menu)
        
        return menu
    }
    
    static func deleteMenuByDescription(description: String)
    {
        // create queue
        var auxiliarQueue:NSOperationQueue = NSOperationQueue()
        
        // create operation
        let deleteOperation : NSBlockOperation = NSBlockOperation(block: {
            // find challenge
            var menu:Menu? = MenuDAO.findByDescription(description)
            if (menu != nil)
            {
                // delete challenge
                MenuDAO.delete(menu!)
            }
        })
        
        // execute operation
        auxiliarQueue.addOperation(deleteOperation)
    }
    
}